// Compiled by ClojureScript 0.0-2234
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function fn_handler(f){if(typeof cljs.core.async.t10841 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10841 = (function (f,fn_handler,meta10842){
this.f = f;
this.fn_handler = fn_handler;
this.meta10842 = meta10842;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10841.cljs$lang$type = true;
cljs.core.async.t10841.cljs$lang$ctorStr = "cljs.core.async/t10841";
cljs.core.async.t10841.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10841");
});
cljs.core.async.t10841.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t10841.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return true;
});
cljs.core.async.t10841.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return self__.f;
});
cljs.core.async.t10841.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_10843){var self__ = this;
var _10843__$1 = this;return self__.meta10842;
});
cljs.core.async.t10841.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_10843,meta10842__$1){var self__ = this;
var _10843__$1 = this;return (new cljs.core.async.t10841(self__.f,self__.fn_handler,meta10842__$1));
});
cljs.core.async.__GT_t10841 = (function __GT_t10841(f__$1,fn_handler__$1,meta10842){return (new cljs.core.async.t10841(f__$1,fn_handler__$1,meta10842));
});
}
return (new cljs.core.async.t10841(f,fn_handler,null));
});
/**
* Returns a fixed buffer of size n. When full, puts will block/park.
*/
cljs.core.async.buffer = (function buffer(n){return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete but
* val will be dropped (no transfer).
*/
cljs.core.async.dropping_buffer = (function dropping_buffer(n){return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete, and be
* buffered, but oldest elements in buffer will be dropped (not
* transferred).
*/
cljs.core.async.sliding_buffer = (function sliding_buffer(n){return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
* Returns true if a channel created with buff will never block. That is to say,
* puts into this buffer will never cause the buffer to be full.
*/
cljs.core.async.unblocking_buffer_QMARK_ = (function unblocking_buffer_QMARK_(buff){var G__10845 = buff;if(G__10845)
{var bit__4188__auto__ = null;if(cljs.core.truth_((function (){var or__3538__auto__ = bit__4188__auto__;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return G__10845.cljs$core$async$impl$protocols$UnblockingBuffer$;
}
})()))
{return true;
} else
{if((!G__10845.cljs$lang$protocol_mask$partition$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__10845);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__10845);
}
});
/**
* Creates a channel with an optional buffer. If buf-or-n is a number,
* will create and use a fixed buffer of that size.
*/
cljs.core.async.chan = (function() {
var chan = null;
var chan__0 = (function (){return chan.call(null,null);
});
var chan__1 = (function (buf_or_n){var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,0))?null:buf_or_n);return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1));
});
chan = function(buf_or_n){
switch(arguments.length){
case 0:
return chan__0.call(this);
case 1:
return chan__1.call(this,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
chan.cljs$core$IFn$_invoke$arity$0 = chan__0;
chan.cljs$core$IFn$_invoke$arity$1 = chan__1;
return chan;
})()
;
/**
* Returns a channel that will close after msecs
*/
cljs.core.async.timeout = (function timeout(msecs){return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
* takes a val from port. Must be called inside a (go ...) block. Will
* return nil if closed. Will park if nothing is available.
*/
cljs.core.async._LT__BANG_ = (function _LT__BANG_(port){if(null)
{return null;
} else
{throw (new Error(("Assert failed: <! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
});
/**
* Asynchronously takes a val from port, passing to fn1. Will pass nil
* if closed. If on-caller? (default true) is true, and value is
* immediately available, will call fn1 on calling thread.
* Returns nil.
*/
cljs.core.async.take_BANG_ = (function() {
var take_BANG_ = null;
var take_BANG___2 = (function (port,fn1){return take_BANG_.call(null,port,fn1,true);
});
var take_BANG___3 = (function (port,fn1,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));if(cljs.core.truth_(ret))
{var val_10846 = cljs.core.deref.call(null,ret);if(cljs.core.truth_(on_caller_QMARK_))
{fn1.call(null,val_10846);
} else
{cljs.core.async.impl.dispatch.run.call(null,((function (val_10846,ret){
return (function (){return fn1.call(null,val_10846);
});})(val_10846,ret))
);
}
} else
{}
return null;
});
take_BANG_ = function(port,fn1,on_caller_QMARK_){
switch(arguments.length){
case 2:
return take_BANG___2.call(this,port,fn1);
case 3:
return take_BANG___3.call(this,port,fn1,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take_BANG_.cljs$core$IFn$_invoke$arity$2 = take_BANG___2;
take_BANG_.cljs$core$IFn$_invoke$arity$3 = take_BANG___3;
return take_BANG_;
})()
;
cljs.core.async.nop = (function nop(){return null;
});
/**
* puts a val into port. nil values are not allowed. Must be called
* inside a (go ...) block. Will park if no buffer space is available.
*/
cljs.core.async._GT__BANG_ = (function _GT__BANG_(port,val){if(null)
{return null;
} else
{throw (new Error(("Assert failed: >! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
});
/**
* Asynchronously puts a val into port, calling fn0 (if supplied) when
* complete. nil values are not allowed. Will throw if closed. If
* on-caller? (default true) is true, and the put is immediately
* accepted, will call fn0 on calling thread.  Returns nil.
*/
cljs.core.async.put_BANG_ = (function() {
var put_BANG_ = null;
var put_BANG___2 = (function (port,val){return put_BANG_.call(null,port,val,cljs.core.async.nop);
});
var put_BANG___3 = (function (port,val,fn0){return put_BANG_.call(null,port,val,fn0,true);
});
var put_BANG___4 = (function (port,val,fn0,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn0));if(cljs.core.truth_((function (){var and__3526__auto__ = ret;if(cljs.core.truth_(and__3526__auto__))
{return cljs.core.not_EQ_.call(null,fn0,cljs.core.async.nop);
} else
{return and__3526__auto__;
}
})()))
{if(cljs.core.truth_(on_caller_QMARK_))
{fn0.call(null);
} else
{cljs.core.async.impl.dispatch.run.call(null,fn0);
}
} else
{}
return null;
});
put_BANG_ = function(port,val,fn0,on_caller_QMARK_){
switch(arguments.length){
case 2:
return put_BANG___2.call(this,port,val);
case 3:
return put_BANG___3.call(this,port,val,fn0);
case 4:
return put_BANG___4.call(this,port,val,fn0,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
put_BANG_.cljs$core$IFn$_invoke$arity$2 = put_BANG___2;
put_BANG_.cljs$core$IFn$_invoke$arity$3 = put_BANG___3;
put_BANG_.cljs$core$IFn$_invoke$arity$4 = put_BANG___4;
return put_BANG_;
})()
;
cljs.core.async.close_BANG_ = (function close_BANG_(port){return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function random_array(n){var a = (new Array(n));var n__4394__auto___10847 = n;var x_10848 = 0;while(true){
if((x_10848 < n__4394__auto___10847))
{(a[x_10848] = 0);
{
var G__10849 = (x_10848 + 1);
x_10848 = G__10849;
continue;
}
} else
{}
break;
}
var i = 1;while(true){
if(cljs.core._EQ_.call(null,i,n))
{return a;
} else
{var j = cljs.core.rand_int.call(null,i);(a[i] = (a[j]));
(a[j] = i);
{
var G__10850 = (i + 1);
i = G__10850;
continue;
}
}
break;
}
});
cljs.core.async.alt_flag = (function alt_flag(){var flag = cljs.core.atom.call(null,true);if(typeof cljs.core.async.t10854 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10854 = (function (flag,alt_flag,meta10855){
this.flag = flag;
this.alt_flag = alt_flag;
this.meta10855 = meta10855;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10854.cljs$lang$type = true;
cljs.core.async.t10854.cljs$lang$ctorStr = "cljs.core.async/t10854";
cljs.core.async.t10854.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10854");
});})(flag))
;
cljs.core.async.t10854.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t10854.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){var self__ = this;
var ___$1 = this;return cljs.core.deref.call(null,self__.flag);
});})(flag))
;
cljs.core.async.t10854.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.flag,null);
return true;
});})(flag))
;
cljs.core.async.t10854.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_10856){var self__ = this;
var _10856__$1 = this;return self__.meta10855;
});})(flag))
;
cljs.core.async.t10854.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_10856,meta10855__$1){var self__ = this;
var _10856__$1 = this;return (new cljs.core.async.t10854(self__.flag,self__.alt_flag,meta10855__$1));
});})(flag))
;
cljs.core.async.__GT_t10854 = ((function (flag){
return (function __GT_t10854(flag__$1,alt_flag__$1,meta10855){return (new cljs.core.async.t10854(flag__$1,alt_flag__$1,meta10855));
});})(flag))
;
}
return (new cljs.core.async.t10854(flag,alt_flag,null));
});
cljs.core.async.alt_handler = (function alt_handler(flag,cb){if(typeof cljs.core.async.t10860 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10860 = (function (cb,flag,alt_handler,meta10861){
this.cb = cb;
this.flag = flag;
this.alt_handler = alt_handler;
this.meta10861 = meta10861;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10860.cljs$lang$type = true;
cljs.core.async.t10860.cljs$lang$ctorStr = "cljs.core.async/t10860";
cljs.core.async.t10860.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10860");
});
cljs.core.async.t10860.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t10860.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});
cljs.core.async.t10860.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;cljs.core.async.impl.protocols.commit.call(null,self__.flag);
return self__.cb;
});
cljs.core.async.t10860.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_10862){var self__ = this;
var _10862__$1 = this;return self__.meta10861;
});
cljs.core.async.t10860.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_10862,meta10861__$1){var self__ = this;
var _10862__$1 = this;return (new cljs.core.async.t10860(self__.cb,self__.flag,self__.alt_handler,meta10861__$1));
});
cljs.core.async.__GT_t10860 = (function __GT_t10860(cb__$1,flag__$1,alt_handler__$1,meta10861){return (new cljs.core.async.t10860(cb__$1,flag__$1,alt_handler__$1,meta10861));
});
}
return (new cljs.core.async.t10860(cb,flag,alt_handler,null));
});
/**
* returns derefable [val port] if immediate, nil if enqueued
*/
cljs.core.async.do_alts = (function do_alts(fret,ports,opts){var flag = cljs.core.async.alt_flag.call(null);var n = cljs.core.count.call(null,ports);var idxs = cljs.core.async.random_array.call(null,n);var priority = new cljs.core.Keyword(null,"priority","priority",4143410454).cljs$core$IFn$_invoke$arity$1(opts);var ret = (function (){var i = 0;while(true){
if((i < n))
{var idx = (cljs.core.truth_(priority)?i:(idxs[i]));var port = cljs.core.nth.call(null,ports,idx);var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,0):null);var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,1);return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (){return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__10863_SHARP_){return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__10863_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));if(cljs.core.truth_(vbox))
{return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__3538__auto__ = wport;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return port;
}
})()], null));
} else
{{
var G__10864 = (i + 1);
i = G__10864;
continue;
}
}
} else
{return null;
}
break;
}
})();var or__3538__auto__ = ret;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",2558708147)))
{var temp__4092__auto__ = (function (){var and__3526__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);if(cljs.core.truth_(and__3526__auto__))
{return cljs.core.async.impl.protocols.commit.call(null,flag);
} else
{return and__3526__auto__;
}
})();if(cljs.core.truth_(temp__4092__auto__))
{var got = temp__4092__auto__;return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",2558708147).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",2558708147)], null));
} else
{return null;
}
} else
{return null;
}
}
});
/**
* Completes at most one of several channel operations. Must be called
* inside a (go ...) block. ports is a vector of channel endpoints, which
* can be either a channel to take from or a vector of
* [channel-to-put-to val-to-put], in any combination. Takes will be
* made as if by <!, and puts will be made as if by >!. Unless
* the :priority option is true, if more than one port operation is
* ready a non-deterministic choice will be made. If no operation is
* ready and a :default value is supplied, [default-val :default] will
* be returned, otherwise alts! will park until the first operation to
* become ready completes. Returns [val port] of the completed
* operation, where val is the value taken for takes, and nil for puts.
* 
* opts are passed as :key val ... Supported options:
* 
* :default val - the value to use if none of the operations are immediately ready
* :priority true - (default nil) when true, the operations will be tried in order.
* 
* Note: there is no guarantee that the port exps or val exprs will be
* used, nor in what order should they be, so they should not be
* depended upon for side effects.
* @param {...*} var_args
*/
cljs.core.async.alts_BANG_ = (function() { 
var alts_BANG___delegate = function (ports,p__10865){var map__10867 = p__10865;var map__10867__$1 = ((cljs.core.seq_QMARK_.call(null,map__10867))?cljs.core.apply.call(null,cljs.core.hash_map,map__10867):map__10867);var opts = map__10867__$1;if(null)
{return null;
} else
{throw (new Error(("Assert failed: alts! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
};
var alts_BANG_ = function (ports,var_args){
var p__10865 = null;if (arguments.length > 1) {
  p__10865 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return alts_BANG___delegate.call(this,ports,p__10865);};
alts_BANG_.cljs$lang$maxFixedArity = 1;
alts_BANG_.cljs$lang$applyTo = (function (arglist__10868){
var ports = cljs.core.first(arglist__10868);
var p__10865 = cljs.core.rest(arglist__10868);
return alts_BANG___delegate(ports,p__10865);
});
alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = alts_BANG___delegate;
return alts_BANG_;
})()
;
/**
* Takes a function and a source channel, and returns a channel which
* contains the values produced by applying f to each value taken from
* the source channel
*/
cljs.core.async.map_LT_ = (function map_LT_(f,ch){if(typeof cljs.core.async.t10876 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10876 = (function (ch,f,map_LT_,meta10877){
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta10877 = meta10877;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10876.cljs$lang$type = true;
cljs.core.async.t10876.cljs$lang$ctorStr = "cljs.core.async/t10876";
cljs.core.async.t10876.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10876");
});
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
});
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){if(typeof cljs.core.async.t10879 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10879 = (function (fn1,_,meta10877,ch,f,map_LT_,meta10880){
this.fn1 = fn1;
this._ = _;
this.meta10877 = meta10877;
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta10880 = meta10880;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10879.cljs$lang$type = true;
cljs.core.async.t10879.cljs$lang$ctorStr = "cljs.core.async/t10879";
cljs.core.async.t10879.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10879");
});})(___$1))
;
cljs.core.async.t10879.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t10879.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;
cljs.core.async.t10879.prototype.cljs$core$async$impl$protocols$Handler$lock_id$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.lock_id.call(null,self__.fn1);
});})(___$1))
;
cljs.core.async.t10879.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);return ((function (f1,___$4,___$1){
return (function (p1__10869_SHARP_){return f1.call(null,(((p1__10869_SHARP_ == null))?null:self__.f.call(null,p1__10869_SHARP_)));
});
;})(f1,___$4,___$1))
});})(___$1))
;
cljs.core.async.t10879.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_10881){var self__ = this;
var _10881__$1 = this;return self__.meta10880;
});})(___$1))
;
cljs.core.async.t10879.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_10881,meta10880__$1){var self__ = this;
var _10881__$1 = this;return (new cljs.core.async.t10879(self__.fn1,self__._,self__.meta10877,self__.ch,self__.f,self__.map_LT_,meta10880__$1));
});})(___$1))
;
cljs.core.async.__GT_t10879 = ((function (___$1){
return (function __GT_t10879(fn1__$1,___$2,meta10877__$1,ch__$2,f__$2,map_LT___$2,meta10880){return (new cljs.core.async.t10879(fn1__$1,___$2,meta10877__$1,ch__$2,f__$2,map_LT___$2,meta10880));
});})(___$1))
;
}
return (new cljs.core.async.t10879(fn1,___$1,self__.meta10877,self__.ch,self__.f,self__.map_LT_,null));
})());if(cljs.core.truth_((function (){var and__3526__auto__ = ret;if(cljs.core.truth_(and__3526__auto__))
{return !((cljs.core.deref.call(null,ret) == null));
} else
{return and__3526__auto__;
}
})()))
{return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else
{return ret;
}
});
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t10876.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t10876.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_10878){var self__ = this;
var _10878__$1 = this;return self__.meta10877;
});
cljs.core.async.t10876.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_10878,meta10877__$1){var self__ = this;
var _10878__$1 = this;return (new cljs.core.async.t10876(self__.ch,self__.f,self__.map_LT_,meta10877__$1));
});
cljs.core.async.__GT_t10876 = (function __GT_t10876(ch__$1,f__$1,map_LT___$1,meta10877){return (new cljs.core.async.t10876(ch__$1,f__$1,map_LT___$1,meta10877));
});
}
return (new cljs.core.async.t10876(ch,f,map_LT_,null));
});
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value before supplying it to the target channel.
*/
cljs.core.async.map_GT_ = (function map_GT_(f,ch){if(typeof cljs.core.async.t10885 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10885 = (function (ch,f,map_GT_,meta10886){
this.ch = ch;
this.f = f;
this.map_GT_ = map_GT_;
this.meta10886 = meta10886;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10885.cljs$lang$type = true;
cljs.core.async.t10885.cljs$lang$ctorStr = "cljs.core.async/t10885";
cljs.core.async.t10885.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10885");
});
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn0);
});
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t10885.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t10885.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_10887){var self__ = this;
var _10887__$1 = this;return self__.meta10886;
});
cljs.core.async.t10885.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_10887,meta10886__$1){var self__ = this;
var _10887__$1 = this;return (new cljs.core.async.t10885(self__.ch,self__.f,self__.map_GT_,meta10886__$1));
});
cljs.core.async.__GT_t10885 = (function __GT_t10885(ch__$1,f__$1,map_GT___$1,meta10886){return (new cljs.core.async.t10885(ch__$1,f__$1,map_GT___$1,meta10886));
});
}
return (new cljs.core.async.t10885(ch,f,map_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns true to the
* target channel.
*/
cljs.core.async.filter_GT_ = (function filter_GT_(p,ch){if(typeof cljs.core.async.t10891 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10891 = (function (ch,p,filter_GT_,meta10892){
this.ch = ch;
this.p = p;
this.filter_GT_ = filter_GT_;
this.meta10892 = meta10892;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10891.cljs$lang$type = true;
cljs.core.async.t10891.cljs$lang$ctorStr = "cljs.core.async/t10891";
cljs.core.async.t10891.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10891");
});
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.p.call(null,val)))
{return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
} else
{return cljs.core.async.impl.channels.box.call(null,null);
}
});
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t10891.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t10891.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_10893){var self__ = this;
var _10893__$1 = this;return self__.meta10892;
});
cljs.core.async.t10891.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_10893,meta10892__$1){var self__ = this;
var _10893__$1 = this;return (new cljs.core.async.t10891(self__.ch,self__.p,self__.filter_GT_,meta10892__$1));
});
cljs.core.async.__GT_t10891 = (function __GT_t10891(ch__$1,p__$1,filter_GT___$1,meta10892){return (new cljs.core.async.t10891(ch__$1,p__$1,filter_GT___$1,meta10892));
});
}
return (new cljs.core.async.t10891(ch,p,filter_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns false to the
* target channel.
*/
cljs.core.async.remove_GT_ = (function remove_GT_(p,ch){return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns true. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.filter_LT_ = (function() {
var filter_LT_ = null;
var filter_LT___2 = (function (p,ch){return filter_LT_.call(null,p,ch,null);
});
var filter_LT___3 = (function (p,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___10976 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___10976,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___10976,out){
return (function (state_10955){var state_val_10956 = (state_10955[1]);if((state_val_10956 === 1))
{var state_10955__$1 = state_10955;var statearr_10957_10977 = state_10955__$1;(statearr_10957_10977[2] = null);
(statearr_10957_10977[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 2))
{var state_10955__$1 = state_10955;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_10955__$1,4,ch);
} else
{if((state_val_10956 === 3))
{var inst_10953 = (state_10955[2]);var state_10955__$1 = state_10955;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_10955__$1,inst_10953);
} else
{if((state_val_10956 === 4))
{var inst_10937 = (state_10955[7]);var inst_10937__$1 = (state_10955[2]);var inst_10938 = (inst_10937__$1 == null);var state_10955__$1 = (function (){var statearr_10958 = state_10955;(statearr_10958[7] = inst_10937__$1);
return statearr_10958;
})();if(cljs.core.truth_(inst_10938))
{var statearr_10959_10978 = state_10955__$1;(statearr_10959_10978[1] = 5);
} else
{var statearr_10960_10979 = state_10955__$1;(statearr_10960_10979[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 5))
{var inst_10940 = cljs.core.async.close_BANG_.call(null,out);var state_10955__$1 = state_10955;var statearr_10961_10980 = state_10955__$1;(statearr_10961_10980[2] = inst_10940);
(statearr_10961_10980[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 6))
{var inst_10937 = (state_10955[7]);var inst_10942 = p.call(null,inst_10937);var state_10955__$1 = state_10955;if(cljs.core.truth_(inst_10942))
{var statearr_10962_10981 = state_10955__$1;(statearr_10962_10981[1] = 8);
} else
{var statearr_10963_10982 = state_10955__$1;(statearr_10963_10982[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 7))
{var inst_10951 = (state_10955[2]);var state_10955__$1 = state_10955;var statearr_10964_10983 = state_10955__$1;(statearr_10964_10983[2] = inst_10951);
(statearr_10964_10983[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 8))
{var inst_10937 = (state_10955[7]);var state_10955__$1 = state_10955;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_10955__$1,11,out,inst_10937);
} else
{if((state_val_10956 === 9))
{var state_10955__$1 = state_10955;var statearr_10965_10984 = state_10955__$1;(statearr_10965_10984[2] = null);
(statearr_10965_10984[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 10))
{var inst_10948 = (state_10955[2]);var state_10955__$1 = (function (){var statearr_10966 = state_10955;(statearr_10966[8] = inst_10948);
return statearr_10966;
})();var statearr_10967_10985 = state_10955__$1;(statearr_10967_10985[2] = null);
(statearr_10967_10985[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10956 === 11))
{var inst_10945 = (state_10955[2]);var state_10955__$1 = state_10955;var statearr_10968_10986 = state_10955__$1;(statearr_10968_10986[2] = inst_10945);
(statearr_10968_10986[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___10976,out))
;return ((function (switch__6331__auto__,c__6346__auto___10976,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_10972 = [null,null,null,null,null,null,null,null,null];(statearr_10972[0] = state_machine__6332__auto__);
(statearr_10972[1] = 1);
return statearr_10972;
});
var state_machine__6332__auto____1 = (function (state_10955){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_10955);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e10973){if((e10973 instanceof Object))
{var ex__6335__auto__ = e10973;var statearr_10974_10987 = state_10955;(statearr_10974_10987[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10955);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e10973;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__10988 = state_10955;
state_10955 = G__10988;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_10955){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_10955);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___10976,out))
})();var state__6348__auto__ = (function (){var statearr_10975 = f__6347__auto__.call(null);(statearr_10975[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___10976);
return statearr_10975;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___10976,out))
);
return out;
});
filter_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return filter_LT___2.call(this,p,ch);
case 3:
return filter_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
filter_LT_.cljs$core$IFn$_invoke$arity$2 = filter_LT___2;
filter_LT_.cljs$core$IFn$_invoke$arity$3 = filter_LT___3;
return filter_LT_;
})()
;
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns false. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.remove_LT_ = (function() {
var remove_LT_ = null;
var remove_LT___2 = (function (p,ch){return remove_LT_.call(null,p,ch,null);
});
var remove_LT___3 = (function (p,ch,buf_or_n){return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});
remove_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return remove_LT___2.call(this,p,ch);
case 3:
return remove_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
remove_LT_.cljs$core$IFn$_invoke$arity$2 = remove_LT___2;
remove_LT_.cljs$core$IFn$_invoke$arity$3 = remove_LT___3;
return remove_LT_;
})()
;
cljs.core.async.mapcat_STAR_ = (function mapcat_STAR_(f,in$,out){var c__6346__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto__){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto__){
return (function (state_11140){var state_val_11141 = (state_11140[1]);if((state_val_11141 === 1))
{var state_11140__$1 = state_11140;var statearr_11142_11179 = state_11140__$1;(statearr_11142_11179[2] = null);
(statearr_11142_11179[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 2))
{var state_11140__$1 = state_11140;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11140__$1,4,in$);
} else
{if((state_val_11141 === 3))
{var inst_11138 = (state_11140[2]);var state_11140__$1 = state_11140;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11140__$1,inst_11138);
} else
{if((state_val_11141 === 4))
{var inst_11086 = (state_11140[7]);var inst_11086__$1 = (state_11140[2]);var inst_11087 = (inst_11086__$1 == null);var state_11140__$1 = (function (){var statearr_11143 = state_11140;(statearr_11143[7] = inst_11086__$1);
return statearr_11143;
})();if(cljs.core.truth_(inst_11087))
{var statearr_11144_11180 = state_11140__$1;(statearr_11144_11180[1] = 5);
} else
{var statearr_11145_11181 = state_11140__$1;(statearr_11145_11181[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 5))
{var inst_11089 = cljs.core.async.close_BANG_.call(null,out);var state_11140__$1 = state_11140;var statearr_11146_11182 = state_11140__$1;(statearr_11146_11182[2] = inst_11089);
(statearr_11146_11182[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 6))
{var inst_11086 = (state_11140[7]);var inst_11091 = f.call(null,inst_11086);var inst_11096 = cljs.core.seq.call(null,inst_11091);var inst_11097 = inst_11096;var inst_11098 = null;var inst_11099 = 0;var inst_11100 = 0;var state_11140__$1 = (function (){var statearr_11147 = state_11140;(statearr_11147[8] = inst_11098);
(statearr_11147[9] = inst_11099);
(statearr_11147[10] = inst_11097);
(statearr_11147[11] = inst_11100);
return statearr_11147;
})();var statearr_11148_11183 = state_11140__$1;(statearr_11148_11183[2] = null);
(statearr_11148_11183[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 7))
{var inst_11136 = (state_11140[2]);var state_11140__$1 = state_11140;var statearr_11149_11184 = state_11140__$1;(statearr_11149_11184[2] = inst_11136);
(statearr_11149_11184[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 8))
{var inst_11099 = (state_11140[9]);var inst_11100 = (state_11140[11]);var inst_11102 = (inst_11100 < inst_11099);var inst_11103 = inst_11102;var state_11140__$1 = state_11140;if(cljs.core.truth_(inst_11103))
{var statearr_11150_11185 = state_11140__$1;(statearr_11150_11185[1] = 10);
} else
{var statearr_11151_11186 = state_11140__$1;(statearr_11151_11186[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 9))
{var inst_11133 = (state_11140[2]);var state_11140__$1 = (function (){var statearr_11152 = state_11140;(statearr_11152[12] = inst_11133);
return statearr_11152;
})();var statearr_11153_11187 = state_11140__$1;(statearr_11153_11187[2] = null);
(statearr_11153_11187[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 10))
{var inst_11098 = (state_11140[8]);var inst_11100 = (state_11140[11]);var inst_11105 = cljs.core._nth.call(null,inst_11098,inst_11100);var state_11140__$1 = state_11140;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11140__$1,13,out,inst_11105);
} else
{if((state_val_11141 === 11))
{var inst_11097 = (state_11140[10]);var inst_11111 = (state_11140[13]);var inst_11111__$1 = cljs.core.seq.call(null,inst_11097);var state_11140__$1 = (function (){var statearr_11157 = state_11140;(statearr_11157[13] = inst_11111__$1);
return statearr_11157;
})();if(inst_11111__$1)
{var statearr_11158_11188 = state_11140__$1;(statearr_11158_11188[1] = 14);
} else
{var statearr_11159_11189 = state_11140__$1;(statearr_11159_11189[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 12))
{var inst_11131 = (state_11140[2]);var state_11140__$1 = state_11140;var statearr_11160_11190 = state_11140__$1;(statearr_11160_11190[2] = inst_11131);
(statearr_11160_11190[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 13))
{var inst_11098 = (state_11140[8]);var inst_11099 = (state_11140[9]);var inst_11097 = (state_11140[10]);var inst_11100 = (state_11140[11]);var inst_11107 = (state_11140[2]);var inst_11108 = (inst_11100 + 1);var tmp11154 = inst_11098;var tmp11155 = inst_11099;var tmp11156 = inst_11097;var inst_11097__$1 = tmp11156;var inst_11098__$1 = tmp11154;var inst_11099__$1 = tmp11155;var inst_11100__$1 = inst_11108;var state_11140__$1 = (function (){var statearr_11161 = state_11140;(statearr_11161[8] = inst_11098__$1);
(statearr_11161[9] = inst_11099__$1);
(statearr_11161[10] = inst_11097__$1);
(statearr_11161[11] = inst_11100__$1);
(statearr_11161[14] = inst_11107);
return statearr_11161;
})();var statearr_11162_11191 = state_11140__$1;(statearr_11162_11191[2] = null);
(statearr_11162_11191[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 14))
{var inst_11111 = (state_11140[13]);var inst_11113 = cljs.core.chunked_seq_QMARK_.call(null,inst_11111);var state_11140__$1 = state_11140;if(inst_11113)
{var statearr_11163_11192 = state_11140__$1;(statearr_11163_11192[1] = 17);
} else
{var statearr_11164_11193 = state_11140__$1;(statearr_11164_11193[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 15))
{var state_11140__$1 = state_11140;var statearr_11165_11194 = state_11140__$1;(statearr_11165_11194[2] = null);
(statearr_11165_11194[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 16))
{var inst_11129 = (state_11140[2]);var state_11140__$1 = state_11140;var statearr_11166_11195 = state_11140__$1;(statearr_11166_11195[2] = inst_11129);
(statearr_11166_11195[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 17))
{var inst_11111 = (state_11140[13]);var inst_11115 = cljs.core.chunk_first.call(null,inst_11111);var inst_11116 = cljs.core.chunk_rest.call(null,inst_11111);var inst_11117 = cljs.core.count.call(null,inst_11115);var inst_11097 = inst_11116;var inst_11098 = inst_11115;var inst_11099 = inst_11117;var inst_11100 = 0;var state_11140__$1 = (function (){var statearr_11167 = state_11140;(statearr_11167[8] = inst_11098);
(statearr_11167[9] = inst_11099);
(statearr_11167[10] = inst_11097);
(statearr_11167[11] = inst_11100);
return statearr_11167;
})();var statearr_11168_11196 = state_11140__$1;(statearr_11168_11196[2] = null);
(statearr_11168_11196[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 18))
{var inst_11111 = (state_11140[13]);var inst_11120 = cljs.core.first.call(null,inst_11111);var state_11140__$1 = state_11140;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11140__$1,20,out,inst_11120);
} else
{if((state_val_11141 === 19))
{var inst_11126 = (state_11140[2]);var state_11140__$1 = state_11140;var statearr_11169_11197 = state_11140__$1;(statearr_11169_11197[2] = inst_11126);
(statearr_11169_11197[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11141 === 20))
{var inst_11111 = (state_11140[13]);var inst_11122 = (state_11140[2]);var inst_11123 = cljs.core.next.call(null,inst_11111);var inst_11097 = inst_11123;var inst_11098 = null;var inst_11099 = 0;var inst_11100 = 0;var state_11140__$1 = (function (){var statearr_11170 = state_11140;(statearr_11170[8] = inst_11098);
(statearr_11170[9] = inst_11099);
(statearr_11170[10] = inst_11097);
(statearr_11170[11] = inst_11100);
(statearr_11170[15] = inst_11122);
return statearr_11170;
})();var statearr_11171_11198 = state_11140__$1;(statearr_11171_11198[2] = null);
(statearr_11171_11198[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto__))
;return ((function (switch__6331__auto__,c__6346__auto__){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_11175 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11175[0] = state_machine__6332__auto__);
(statearr_11175[1] = 1);
return statearr_11175;
});
var state_machine__6332__auto____1 = (function (state_11140){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11140);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e11176){if((e11176 instanceof Object))
{var ex__6335__auto__ = e11176;var statearr_11177_11199 = state_11140;(statearr_11177_11199[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11140);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11176;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11200 = state_11140;
state_11140 = G__11200;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11140){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11140);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto__))
})();var state__6348__auto__ = (function (){var statearr_11178 = f__6347__auto__.call(null);(statearr_11178[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto__);
return statearr_11178;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto__))
);
return c__6346__auto__;
});
/**
* Takes a function and a source channel, and returns a channel which
* contains the values in each collection produced by applying f to
* each value taken from the source channel. f must return a
* collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The channel will close when the source channel
* closes.
*/
cljs.core.async.mapcat_LT_ = (function() {
var mapcat_LT_ = null;
var mapcat_LT___2 = (function (f,in$){return mapcat_LT_.call(null,f,in$,null);
});
var mapcat_LT___3 = (function (f,in$,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return out;
});
mapcat_LT_ = function(f,in$,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_LT___2.call(this,f,in$);
case 3:
return mapcat_LT___3.call(this,f,in$,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = mapcat_LT___2;
mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = mapcat_LT___3;
return mapcat_LT_;
})()
;
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value put, then supplies each element of the result
* to the target channel. f must return a collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The target channel will be closed when the source
* channel closes.
*/
cljs.core.async.mapcat_GT_ = (function() {
var mapcat_GT_ = null;
var mapcat_GT___2 = (function (f,out){return mapcat_GT_.call(null,f,out,null);
});
var mapcat_GT___3 = (function (f,out,buf_or_n){var in$ = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return in$;
});
mapcat_GT_ = function(f,out,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_GT___2.call(this,f,out);
case 3:
return mapcat_GT___3.call(this,f,out,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = mapcat_GT___2;
mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = mapcat_GT___3;
return mapcat_GT_;
})()
;
/**
* Takes elements from the from channel and supplies them to the to
* channel. By default, the to channel will be closed when the
* from channel closes, but can be determined by the close?
* parameter.
*/
cljs.core.async.pipe = (function() {
var pipe = null;
var pipe__2 = (function (from,to){return pipe.call(null,from,to,true);
});
var pipe__3 = (function (from,to,close_QMARK_){var c__6346__auto___11281 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___11281){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___11281){
return (function (state_11260){var state_val_11261 = (state_11260[1]);if((state_val_11261 === 1))
{var state_11260__$1 = state_11260;var statearr_11262_11282 = state_11260__$1;(statearr_11262_11282[2] = null);
(statearr_11262_11282[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 2))
{var state_11260__$1 = state_11260;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11260__$1,4,from);
} else
{if((state_val_11261 === 3))
{var inst_11258 = (state_11260[2]);var state_11260__$1 = state_11260;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11260__$1,inst_11258);
} else
{if((state_val_11261 === 4))
{var inst_11243 = (state_11260[7]);var inst_11243__$1 = (state_11260[2]);var inst_11244 = (inst_11243__$1 == null);var state_11260__$1 = (function (){var statearr_11263 = state_11260;(statearr_11263[7] = inst_11243__$1);
return statearr_11263;
})();if(cljs.core.truth_(inst_11244))
{var statearr_11264_11283 = state_11260__$1;(statearr_11264_11283[1] = 5);
} else
{var statearr_11265_11284 = state_11260__$1;(statearr_11265_11284[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 5))
{var state_11260__$1 = state_11260;if(cljs.core.truth_(close_QMARK_))
{var statearr_11266_11285 = state_11260__$1;(statearr_11266_11285[1] = 8);
} else
{var statearr_11267_11286 = state_11260__$1;(statearr_11267_11286[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 6))
{var inst_11243 = (state_11260[7]);var state_11260__$1 = state_11260;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11260__$1,11,to,inst_11243);
} else
{if((state_val_11261 === 7))
{var inst_11256 = (state_11260[2]);var state_11260__$1 = state_11260;var statearr_11268_11287 = state_11260__$1;(statearr_11268_11287[2] = inst_11256);
(statearr_11268_11287[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 8))
{var inst_11247 = cljs.core.async.close_BANG_.call(null,to);var state_11260__$1 = state_11260;var statearr_11269_11288 = state_11260__$1;(statearr_11269_11288[2] = inst_11247);
(statearr_11269_11288[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 9))
{var state_11260__$1 = state_11260;var statearr_11270_11289 = state_11260__$1;(statearr_11270_11289[2] = null);
(statearr_11270_11289[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 10))
{var inst_11250 = (state_11260[2]);var state_11260__$1 = state_11260;var statearr_11271_11290 = state_11260__$1;(statearr_11271_11290[2] = inst_11250);
(statearr_11271_11290[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11261 === 11))
{var inst_11253 = (state_11260[2]);var state_11260__$1 = (function (){var statearr_11272 = state_11260;(statearr_11272[8] = inst_11253);
return statearr_11272;
})();var statearr_11273_11291 = state_11260__$1;(statearr_11273_11291[2] = null);
(statearr_11273_11291[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___11281))
;return ((function (switch__6331__auto__,c__6346__auto___11281){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_11277 = [null,null,null,null,null,null,null,null,null];(statearr_11277[0] = state_machine__6332__auto__);
(statearr_11277[1] = 1);
return statearr_11277;
});
var state_machine__6332__auto____1 = (function (state_11260){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11260);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e11278){if((e11278 instanceof Object))
{var ex__6335__auto__ = e11278;var statearr_11279_11292 = state_11260;(statearr_11279_11292[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11260);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11278;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11293 = state_11260;
state_11260 = G__11293;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11260){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11260);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___11281))
})();var state__6348__auto__ = (function (){var statearr_11280 = f__6347__auto__.call(null);(statearr_11280[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___11281);
return statearr_11280;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___11281))
);
return to;
});
pipe = function(from,to,close_QMARK_){
switch(arguments.length){
case 2:
return pipe__2.call(this,from,to);
case 3:
return pipe__3.call(this,from,to,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipe.cljs$core$IFn$_invoke$arity$2 = pipe__2;
pipe.cljs$core$IFn$_invoke$arity$3 = pipe__3;
return pipe;
})()
;
/**
* Takes a predicate and a source channel and returns a vector of two
* channels, the first of which will contain the values for which the
* predicate returned true, the second those for which it returned
* false.
* 
* The out channels will be unbuffered by default, or two buf-or-ns can
* be supplied. The channels will close after the source channel has
* closed.
*/
cljs.core.async.split = (function() {
var split = null;
var split__2 = (function (p,ch){return split.call(null,p,ch,null,null);
});
var split__4 = (function (p,ch,t_buf_or_n,f_buf_or_n){var tc = cljs.core.async.chan.call(null,t_buf_or_n);var fc = cljs.core.async.chan.call(null,f_buf_or_n);var c__6346__auto___11380 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___11380,tc,fc){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___11380,tc,fc){
return (function (state_11358){var state_val_11359 = (state_11358[1]);if((state_val_11359 === 1))
{var state_11358__$1 = state_11358;var statearr_11360_11381 = state_11358__$1;(statearr_11360_11381[2] = null);
(statearr_11360_11381[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 2))
{var state_11358__$1 = state_11358;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11358__$1,4,ch);
} else
{if((state_val_11359 === 3))
{var inst_11356 = (state_11358[2]);var state_11358__$1 = state_11358;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11358__$1,inst_11356);
} else
{if((state_val_11359 === 4))
{var inst_11339 = (state_11358[7]);var inst_11339__$1 = (state_11358[2]);var inst_11340 = (inst_11339__$1 == null);var state_11358__$1 = (function (){var statearr_11361 = state_11358;(statearr_11361[7] = inst_11339__$1);
return statearr_11361;
})();if(cljs.core.truth_(inst_11340))
{var statearr_11362_11382 = state_11358__$1;(statearr_11362_11382[1] = 5);
} else
{var statearr_11363_11383 = state_11358__$1;(statearr_11363_11383[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 5))
{var inst_11342 = cljs.core.async.close_BANG_.call(null,tc);var inst_11343 = cljs.core.async.close_BANG_.call(null,fc);var state_11358__$1 = (function (){var statearr_11364 = state_11358;(statearr_11364[8] = inst_11342);
return statearr_11364;
})();var statearr_11365_11384 = state_11358__$1;(statearr_11365_11384[2] = inst_11343);
(statearr_11365_11384[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 6))
{var inst_11339 = (state_11358[7]);var inst_11345 = p.call(null,inst_11339);var state_11358__$1 = state_11358;if(cljs.core.truth_(inst_11345))
{var statearr_11366_11385 = state_11358__$1;(statearr_11366_11385[1] = 9);
} else
{var statearr_11367_11386 = state_11358__$1;(statearr_11367_11386[1] = 10);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 7))
{var inst_11354 = (state_11358[2]);var state_11358__$1 = state_11358;var statearr_11368_11387 = state_11358__$1;(statearr_11368_11387[2] = inst_11354);
(statearr_11368_11387[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 8))
{var inst_11351 = (state_11358[2]);var state_11358__$1 = (function (){var statearr_11369 = state_11358;(statearr_11369[9] = inst_11351);
return statearr_11369;
})();var statearr_11370_11388 = state_11358__$1;(statearr_11370_11388[2] = null);
(statearr_11370_11388[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 9))
{var state_11358__$1 = state_11358;var statearr_11371_11389 = state_11358__$1;(statearr_11371_11389[2] = tc);
(statearr_11371_11389[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 10))
{var state_11358__$1 = state_11358;var statearr_11372_11390 = state_11358__$1;(statearr_11372_11390[2] = fc);
(statearr_11372_11390[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11359 === 11))
{var inst_11339 = (state_11358[7]);var inst_11349 = (state_11358[2]);var state_11358__$1 = state_11358;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11358__$1,8,inst_11349,inst_11339);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___11380,tc,fc))
;return ((function (switch__6331__auto__,c__6346__auto___11380,tc,fc){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_11376 = [null,null,null,null,null,null,null,null,null,null];(statearr_11376[0] = state_machine__6332__auto__);
(statearr_11376[1] = 1);
return statearr_11376;
});
var state_machine__6332__auto____1 = (function (state_11358){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11358);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e11377){if((e11377 instanceof Object))
{var ex__6335__auto__ = e11377;var statearr_11378_11391 = state_11358;(statearr_11378_11391[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11358);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11377;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11392 = state_11358;
state_11358 = G__11392;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11358){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11358);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___11380,tc,fc))
})();var state__6348__auto__ = (function (){var statearr_11379 = f__6347__auto__.call(null);(statearr_11379[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___11380);
return statearr_11379;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___11380,tc,fc))
);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});
split = function(p,ch,t_buf_or_n,f_buf_or_n){
switch(arguments.length){
case 2:
return split__2.call(this,p,ch);
case 4:
return split__4.call(this,p,ch,t_buf_or_n,f_buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
split.cljs$core$IFn$_invoke$arity$2 = split__2;
split.cljs$core$IFn$_invoke$arity$4 = split__4;
return split;
})()
;
/**
* f should be a function of 2 arguments. Returns a channel containing
* the single result of applying f to init and the first item from the
* channel, then applying f to that result and the 2nd item, etc. If
* the channel closes without yielding items, returns init and f is not
* called. ch must close before reduce produces a result.
*/
cljs.core.async.reduce = (function reduce(f,init,ch){var c__6346__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto__){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto__){
return (function (state_11439){var state_val_11440 = (state_11439[1]);if((state_val_11440 === 7))
{var inst_11435 = (state_11439[2]);var state_11439__$1 = state_11439;var statearr_11441_11457 = state_11439__$1;(statearr_11441_11457[2] = inst_11435);
(statearr_11441_11457[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11440 === 6))
{var inst_11428 = (state_11439[7]);var inst_11425 = (state_11439[8]);var inst_11432 = f.call(null,inst_11425,inst_11428);var inst_11425__$1 = inst_11432;var state_11439__$1 = (function (){var statearr_11442 = state_11439;(statearr_11442[8] = inst_11425__$1);
return statearr_11442;
})();var statearr_11443_11458 = state_11439__$1;(statearr_11443_11458[2] = null);
(statearr_11443_11458[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11440 === 5))
{var inst_11425 = (state_11439[8]);var state_11439__$1 = state_11439;var statearr_11444_11459 = state_11439__$1;(statearr_11444_11459[2] = inst_11425);
(statearr_11444_11459[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11440 === 4))
{var inst_11428 = (state_11439[7]);var inst_11428__$1 = (state_11439[2]);var inst_11429 = (inst_11428__$1 == null);var state_11439__$1 = (function (){var statearr_11445 = state_11439;(statearr_11445[7] = inst_11428__$1);
return statearr_11445;
})();if(cljs.core.truth_(inst_11429))
{var statearr_11446_11460 = state_11439__$1;(statearr_11446_11460[1] = 5);
} else
{var statearr_11447_11461 = state_11439__$1;(statearr_11447_11461[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11440 === 3))
{var inst_11437 = (state_11439[2]);var state_11439__$1 = state_11439;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11439__$1,inst_11437);
} else
{if((state_val_11440 === 2))
{var state_11439__$1 = state_11439;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11439__$1,4,ch);
} else
{if((state_val_11440 === 1))
{var inst_11425 = init;var state_11439__$1 = (function (){var statearr_11448 = state_11439;(statearr_11448[8] = inst_11425);
return statearr_11448;
})();var statearr_11449_11462 = state_11439__$1;(statearr_11449_11462[2] = null);
(statearr_11449_11462[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
});})(c__6346__auto__))
;return ((function (switch__6331__auto__,c__6346__auto__){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_11453 = [null,null,null,null,null,null,null,null,null];(statearr_11453[0] = state_machine__6332__auto__);
(statearr_11453[1] = 1);
return statearr_11453;
});
var state_machine__6332__auto____1 = (function (state_11439){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11439);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e11454){if((e11454 instanceof Object))
{var ex__6335__auto__ = e11454;var statearr_11455_11463 = state_11439;(statearr_11455_11463[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11439);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11454;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11464 = state_11439;
state_11439 = G__11464;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11439){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11439);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto__))
})();var state__6348__auto__ = (function (){var statearr_11456 = f__6347__auto__.call(null);(statearr_11456[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto__);
return statearr_11456;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto__))
);
return c__6346__auto__;
});
/**
* Puts the contents of coll into the supplied channel.
* 
* By default the channel will be closed after the items are copied,
* but can be determined by the close? parameter.
* 
* Returns a channel which will close after the items are copied.
*/
cljs.core.async.onto_chan = (function() {
var onto_chan = null;
var onto_chan__2 = (function (ch,coll){return onto_chan.call(null,ch,coll,true);
});
var onto_chan__3 = (function (ch,coll,close_QMARK_){var c__6346__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto__){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto__){
return (function (state_11526){var state_val_11527 = (state_11526[1]);if((state_val_11527 === 1))
{var inst_11506 = cljs.core.seq.call(null,coll);var inst_11507 = inst_11506;var state_11526__$1 = (function (){var statearr_11528 = state_11526;(statearr_11528[7] = inst_11507);
return statearr_11528;
})();var statearr_11529_11547 = state_11526__$1;(statearr_11529_11547[2] = null);
(statearr_11529_11547[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 2))
{var inst_11507 = (state_11526[7]);var state_11526__$1 = state_11526;if(cljs.core.truth_(inst_11507))
{var statearr_11530_11548 = state_11526__$1;(statearr_11530_11548[1] = 4);
} else
{var statearr_11531_11549 = state_11526__$1;(statearr_11531_11549[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 3))
{var inst_11524 = (state_11526[2]);var state_11526__$1 = state_11526;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11526__$1,inst_11524);
} else
{if((state_val_11527 === 4))
{var inst_11507 = (state_11526[7]);var inst_11510 = cljs.core.first.call(null,inst_11507);var state_11526__$1 = state_11526;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11526__$1,7,ch,inst_11510);
} else
{if((state_val_11527 === 5))
{var state_11526__$1 = state_11526;if(cljs.core.truth_(close_QMARK_))
{var statearr_11532_11550 = state_11526__$1;(statearr_11532_11550[1] = 8);
} else
{var statearr_11533_11551 = state_11526__$1;(statearr_11533_11551[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 6))
{var inst_11522 = (state_11526[2]);var state_11526__$1 = state_11526;var statearr_11534_11552 = state_11526__$1;(statearr_11534_11552[2] = inst_11522);
(statearr_11534_11552[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 7))
{var inst_11507 = (state_11526[7]);var inst_11512 = (state_11526[2]);var inst_11513 = cljs.core.next.call(null,inst_11507);var inst_11507__$1 = inst_11513;var state_11526__$1 = (function (){var statearr_11535 = state_11526;(statearr_11535[7] = inst_11507__$1);
(statearr_11535[8] = inst_11512);
return statearr_11535;
})();var statearr_11536_11553 = state_11526__$1;(statearr_11536_11553[2] = null);
(statearr_11536_11553[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 8))
{var inst_11517 = cljs.core.async.close_BANG_.call(null,ch);var state_11526__$1 = state_11526;var statearr_11537_11554 = state_11526__$1;(statearr_11537_11554[2] = inst_11517);
(statearr_11537_11554[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 9))
{var state_11526__$1 = state_11526;var statearr_11538_11555 = state_11526__$1;(statearr_11538_11555[2] = null);
(statearr_11538_11555[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11527 === 10))
{var inst_11520 = (state_11526[2]);var state_11526__$1 = state_11526;var statearr_11539_11556 = state_11526__$1;(statearr_11539_11556[2] = inst_11520);
(statearr_11539_11556[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto__))
;return ((function (switch__6331__auto__,c__6346__auto__){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_11543 = [null,null,null,null,null,null,null,null,null];(statearr_11543[0] = state_machine__6332__auto__);
(statearr_11543[1] = 1);
return statearr_11543;
});
var state_machine__6332__auto____1 = (function (state_11526){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11526);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e11544){if((e11544 instanceof Object))
{var ex__6335__auto__ = e11544;var statearr_11545_11557 = state_11526;(statearr_11545_11557[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11526);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11544;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11558 = state_11526;
state_11526 = G__11558;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11526){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11526);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto__))
})();var state__6348__auto__ = (function (){var statearr_11546 = f__6347__auto__.call(null);(statearr_11546[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto__);
return statearr_11546;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto__))
);
return c__6346__auto__;
});
onto_chan = function(ch,coll,close_QMARK_){
switch(arguments.length){
case 2:
return onto_chan__2.call(this,ch,coll);
case 3:
return onto_chan__3.call(this,ch,coll,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
onto_chan.cljs$core$IFn$_invoke$arity$2 = onto_chan__2;
onto_chan.cljs$core$IFn$_invoke$arity$3 = onto_chan__3;
return onto_chan;
})()
;
/**
* Creates and returns a channel which contains the contents of coll,
* closing when exhausted.
*/
cljs.core.async.to_chan = (function to_chan(coll){var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,100,coll));cljs.core.async.onto_chan.call(null,ch,coll);
return ch;
});
cljs.core.async.Mux = (function (){var obj11560 = {};return obj11560;
})();
cljs.core.async.muxch_STAR_ = (function muxch_STAR_(_){if((function (){var and__3526__auto__ = _;if(and__3526__auto__)
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else
{var x__4165__auto__ = (((_ == null))?null:_);return (function (){var or__3538__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
})().call(null,_);
}
});
cljs.core.async.Mult = (function (){var obj11562 = {};return obj11562;
})();
cljs.core.async.tap_STAR_ = (function tap_STAR_(m,ch,close_QMARK_){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$tap_STAR_$arity$3;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.tap_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
})().call(null,m,ch,close_QMARK_);
}
});
cljs.core.async.untap_STAR_ = (function untap_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$untap_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.untap_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.untap_all_STAR_ = (function untap_all_STAR_(m){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
})().call(null,m);
}
});
/**
* Creates and returns a mult(iple) of the supplied channel. Channels
* containing copies of the channel can be created with 'tap', and
* detached with 'untap'.
* 
* Each item is distributed to all taps in parallel and synchronously,
* i.e. each tap must accept before the next item is distributed. Use
* buffering/windowing to prevent slow taps from holding up the mult.
* 
* Items received when there are no taps get dropped.
* 
* If a tap put throws an exception, it will be removed from the mult.
*/
cljs.core.async.mult = (function mult(ch){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var m = (function (){if(typeof cljs.core.async.t11786 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t11786 = (function (cs,ch,mult,meta11787){
this.cs = cs;
this.ch = ch;
this.mult = mult;
this.meta11787 = meta11787;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t11786.cljs$lang$type = true;
cljs.core.async.t11786.cljs$lang$ctorStr = "cljs.core.async/t11786";
cljs.core.async.t11786.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t11786");
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$async$Mult$ = true;
cljs.core.async.t11786.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$2,close_QMARK_){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$2,close_QMARK_);
return null;
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$2){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$2);
return null;
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return null;
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t11786.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_11788){var self__ = this;
var _11788__$1 = this;return self__.meta11787;
});})(cs))
;
cljs.core.async.t11786.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_11788,meta11787__$1){var self__ = this;
var _11788__$1 = this;return (new cljs.core.async.t11786(self__.cs,self__.ch,self__.mult,meta11787__$1));
});})(cs))
;
cljs.core.async.__GT_t11786 = ((function (cs){
return (function __GT_t11786(cs__$1,ch__$1,mult__$1,meta11787){return (new cljs.core.async.t11786(cs__$1,ch__$1,mult__$1,meta11787));
});})(cs))
;
}
return (new cljs.core.async.t11786(cs,ch,mult,null));
})();var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = ((function (cs,m,dchan,dctr){
return (function (){if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,true);
} else
{return null;
}
});})(cs,m,dchan,dctr))
;var c__6346__auto___12009 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12009,cs,m,dchan,dctr,done){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12009,cs,m,dchan,dctr,done){
return (function (state_11923){var state_val_11924 = (state_11923[1]);if((state_val_11924 === 32))
{var inst_11867 = (state_11923[7]);var inst_11791 = (state_11923[8]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_11923,31,Object,null,30);var inst_11874 = cljs.core.async.put_BANG_.call(null,inst_11867,inst_11791,done);var state_11923__$1 = state_11923;var statearr_11925_12010 = state_11923__$1;(statearr_11925_12010[2] = inst_11874);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11923__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 1))
{var state_11923__$1 = state_11923;var statearr_11926_12011 = state_11923__$1;(statearr_11926_12011[2] = null);
(statearr_11926_12011[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 33))
{var inst_11880 = (state_11923[9]);var inst_11882 = cljs.core.chunked_seq_QMARK_.call(null,inst_11880);var state_11923__$1 = state_11923;if(inst_11882)
{var statearr_11927_12012 = state_11923__$1;(statearr_11927_12012[1] = 36);
} else
{var statearr_11928_12013 = state_11923__$1;(statearr_11928_12013[1] = 37);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 2))
{var state_11923__$1 = state_11923;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11923__$1,4,ch);
} else
{if((state_val_11924 === 34))
{var state_11923__$1 = state_11923;var statearr_11929_12014 = state_11923__$1;(statearr_11929_12014[2] = null);
(statearr_11929_12014[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 3))
{var inst_11921 = (state_11923[2]);var state_11923__$1 = state_11923;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11923__$1,inst_11921);
} else
{if((state_val_11924 === 35))
{var inst_11905 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11930_12015 = state_11923__$1;(statearr_11930_12015[2] = inst_11905);
(statearr_11930_12015[1] = 29);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 4))
{var inst_11791 = (state_11923[8]);var inst_11791__$1 = (state_11923[2]);var inst_11792 = (inst_11791__$1 == null);var state_11923__$1 = (function (){var statearr_11931 = state_11923;(statearr_11931[8] = inst_11791__$1);
return statearr_11931;
})();if(cljs.core.truth_(inst_11792))
{var statearr_11932_12016 = state_11923__$1;(statearr_11932_12016[1] = 5);
} else
{var statearr_11933_12017 = state_11923__$1;(statearr_11933_12017[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 36))
{var inst_11880 = (state_11923[9]);var inst_11884 = cljs.core.chunk_first.call(null,inst_11880);var inst_11885 = cljs.core.chunk_rest.call(null,inst_11880);var inst_11886 = cljs.core.count.call(null,inst_11884);var inst_11859 = inst_11885;var inst_11860 = inst_11884;var inst_11861 = inst_11886;var inst_11862 = 0;var state_11923__$1 = (function (){var statearr_11934 = state_11923;(statearr_11934[10] = inst_11862);
(statearr_11934[11] = inst_11861);
(statearr_11934[12] = inst_11860);
(statearr_11934[13] = inst_11859);
return statearr_11934;
})();var statearr_11935_12018 = state_11923__$1;(statearr_11935_12018[2] = null);
(statearr_11935_12018[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 5))
{var inst_11798 = cljs.core.deref.call(null,cs);var inst_11799 = cljs.core.seq.call(null,inst_11798);var inst_11800 = inst_11799;var inst_11801 = null;var inst_11802 = 0;var inst_11803 = 0;var state_11923__$1 = (function (){var statearr_11936 = state_11923;(statearr_11936[14] = inst_11800);
(statearr_11936[15] = inst_11802);
(statearr_11936[16] = inst_11801);
(statearr_11936[17] = inst_11803);
return statearr_11936;
})();var statearr_11937_12019 = state_11923__$1;(statearr_11937_12019[2] = null);
(statearr_11937_12019[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 37))
{var inst_11880 = (state_11923[9]);var inst_11889 = cljs.core.first.call(null,inst_11880);var state_11923__$1 = (function (){var statearr_11938 = state_11923;(statearr_11938[18] = inst_11889);
return statearr_11938;
})();var statearr_11939_12020 = state_11923__$1;(statearr_11939_12020[2] = null);
(statearr_11939_12020[1] = 41);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 6))
{var inst_11851 = (state_11923[19]);var inst_11850 = cljs.core.deref.call(null,cs);var inst_11851__$1 = cljs.core.keys.call(null,inst_11850);var inst_11852 = cljs.core.count.call(null,inst_11851__$1);var inst_11853 = cljs.core.reset_BANG_.call(null,dctr,inst_11852);var inst_11858 = cljs.core.seq.call(null,inst_11851__$1);var inst_11859 = inst_11858;var inst_11860 = null;var inst_11861 = 0;var inst_11862 = 0;var state_11923__$1 = (function (){var statearr_11940 = state_11923;(statearr_11940[10] = inst_11862);
(statearr_11940[11] = inst_11861);
(statearr_11940[20] = inst_11853);
(statearr_11940[12] = inst_11860);
(statearr_11940[19] = inst_11851__$1);
(statearr_11940[13] = inst_11859);
return statearr_11940;
})();var statearr_11941_12021 = state_11923__$1;(statearr_11941_12021[2] = null);
(statearr_11941_12021[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 38))
{var inst_11902 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11942_12022 = state_11923__$1;(statearr_11942_12022[2] = inst_11902);
(statearr_11942_12022[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 7))
{var inst_11919 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11943_12023 = state_11923__$1;(statearr_11943_12023[2] = inst_11919);
(statearr_11943_12023[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 39))
{var inst_11880 = (state_11923[9]);var inst_11898 = (state_11923[2]);var inst_11899 = cljs.core.next.call(null,inst_11880);var inst_11859 = inst_11899;var inst_11860 = null;var inst_11861 = 0;var inst_11862 = 0;var state_11923__$1 = (function (){var statearr_11944 = state_11923;(statearr_11944[10] = inst_11862);
(statearr_11944[11] = inst_11861);
(statearr_11944[12] = inst_11860);
(statearr_11944[21] = inst_11898);
(statearr_11944[13] = inst_11859);
return statearr_11944;
})();var statearr_11945_12024 = state_11923__$1;(statearr_11945_12024[2] = null);
(statearr_11945_12024[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 8))
{var inst_11802 = (state_11923[15]);var inst_11803 = (state_11923[17]);var inst_11805 = (inst_11803 < inst_11802);var inst_11806 = inst_11805;var state_11923__$1 = state_11923;if(cljs.core.truth_(inst_11806))
{var statearr_11946_12025 = state_11923__$1;(statearr_11946_12025[1] = 10);
} else
{var statearr_11947_12026 = state_11923__$1;(statearr_11947_12026[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 40))
{var inst_11889 = (state_11923[18]);var inst_11890 = (state_11923[2]);var inst_11891 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_11892 = cljs.core.async.untap_STAR_.call(null,m,inst_11889);var state_11923__$1 = (function (){var statearr_11948 = state_11923;(statearr_11948[22] = inst_11890);
(statearr_11948[23] = inst_11891);
return statearr_11948;
})();var statearr_11949_12027 = state_11923__$1;(statearr_11949_12027[2] = inst_11892);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11923__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 9))
{var inst_11848 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11950_12028 = state_11923__$1;(statearr_11950_12028[2] = inst_11848);
(statearr_11950_12028[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 41))
{var inst_11791 = (state_11923[8]);var inst_11889 = (state_11923[18]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_11923,40,Object,null,39);var inst_11896 = cljs.core.async.put_BANG_.call(null,inst_11889,inst_11791,done);var state_11923__$1 = state_11923;var statearr_11951_12029 = state_11923__$1;(statearr_11951_12029[2] = inst_11896);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11923__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 10))
{var inst_11801 = (state_11923[16]);var inst_11803 = (state_11923[17]);var inst_11809 = cljs.core._nth.call(null,inst_11801,inst_11803);var inst_11810 = cljs.core.nth.call(null,inst_11809,0,null);var inst_11811 = cljs.core.nth.call(null,inst_11809,1,null);var state_11923__$1 = (function (){var statearr_11952 = state_11923;(statearr_11952[24] = inst_11810);
return statearr_11952;
})();if(cljs.core.truth_(inst_11811))
{var statearr_11953_12030 = state_11923__$1;(statearr_11953_12030[1] = 13);
} else
{var statearr_11954_12031 = state_11923__$1;(statearr_11954_12031[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 42))
{var state_11923__$1 = state_11923;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11923__$1,45,dchan);
} else
{if((state_val_11924 === 11))
{var inst_11800 = (state_11923[14]);var inst_11820 = (state_11923[25]);var inst_11820__$1 = cljs.core.seq.call(null,inst_11800);var state_11923__$1 = (function (){var statearr_11955 = state_11923;(statearr_11955[25] = inst_11820__$1);
return statearr_11955;
})();if(inst_11820__$1)
{var statearr_11956_12032 = state_11923__$1;(statearr_11956_12032[1] = 16);
} else
{var statearr_11957_12033 = state_11923__$1;(statearr_11957_12033[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 43))
{var state_11923__$1 = state_11923;var statearr_11958_12034 = state_11923__$1;(statearr_11958_12034[2] = null);
(statearr_11958_12034[1] = 44);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 12))
{var inst_11846 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11959_12035 = state_11923__$1;(statearr_11959_12035[2] = inst_11846);
(statearr_11959_12035[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 44))
{var inst_11916 = (state_11923[2]);var state_11923__$1 = (function (){var statearr_11960 = state_11923;(statearr_11960[26] = inst_11916);
return statearr_11960;
})();var statearr_11961_12036 = state_11923__$1;(statearr_11961_12036[2] = null);
(statearr_11961_12036[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 13))
{var inst_11810 = (state_11923[24]);var inst_11813 = cljs.core.async.close_BANG_.call(null,inst_11810);var state_11923__$1 = state_11923;var statearr_11962_12037 = state_11923__$1;(statearr_11962_12037[2] = inst_11813);
(statearr_11962_12037[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 45))
{var inst_11913 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11966_12038 = state_11923__$1;(statearr_11966_12038[2] = inst_11913);
(statearr_11966_12038[1] = 44);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 14))
{var state_11923__$1 = state_11923;var statearr_11967_12039 = state_11923__$1;(statearr_11967_12039[2] = null);
(statearr_11967_12039[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 15))
{var inst_11800 = (state_11923[14]);var inst_11802 = (state_11923[15]);var inst_11801 = (state_11923[16]);var inst_11803 = (state_11923[17]);var inst_11816 = (state_11923[2]);var inst_11817 = (inst_11803 + 1);var tmp11963 = inst_11800;var tmp11964 = inst_11802;var tmp11965 = inst_11801;var inst_11800__$1 = tmp11963;var inst_11801__$1 = tmp11965;var inst_11802__$1 = tmp11964;var inst_11803__$1 = inst_11817;var state_11923__$1 = (function (){var statearr_11968 = state_11923;(statearr_11968[14] = inst_11800__$1);
(statearr_11968[15] = inst_11802__$1);
(statearr_11968[16] = inst_11801__$1);
(statearr_11968[17] = inst_11803__$1);
(statearr_11968[27] = inst_11816);
return statearr_11968;
})();var statearr_11969_12040 = state_11923__$1;(statearr_11969_12040[2] = null);
(statearr_11969_12040[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 16))
{var inst_11820 = (state_11923[25]);var inst_11822 = cljs.core.chunked_seq_QMARK_.call(null,inst_11820);var state_11923__$1 = state_11923;if(inst_11822)
{var statearr_11970_12041 = state_11923__$1;(statearr_11970_12041[1] = 19);
} else
{var statearr_11971_12042 = state_11923__$1;(statearr_11971_12042[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 17))
{var state_11923__$1 = state_11923;var statearr_11972_12043 = state_11923__$1;(statearr_11972_12043[2] = null);
(statearr_11972_12043[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 18))
{var inst_11844 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11973_12044 = state_11923__$1;(statearr_11973_12044[2] = inst_11844);
(statearr_11973_12044[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 19))
{var inst_11820 = (state_11923[25]);var inst_11824 = cljs.core.chunk_first.call(null,inst_11820);var inst_11825 = cljs.core.chunk_rest.call(null,inst_11820);var inst_11826 = cljs.core.count.call(null,inst_11824);var inst_11800 = inst_11825;var inst_11801 = inst_11824;var inst_11802 = inst_11826;var inst_11803 = 0;var state_11923__$1 = (function (){var statearr_11974 = state_11923;(statearr_11974[14] = inst_11800);
(statearr_11974[15] = inst_11802);
(statearr_11974[16] = inst_11801);
(statearr_11974[17] = inst_11803);
return statearr_11974;
})();var statearr_11975_12045 = state_11923__$1;(statearr_11975_12045[2] = null);
(statearr_11975_12045[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 20))
{var inst_11820 = (state_11923[25]);var inst_11830 = cljs.core.first.call(null,inst_11820);var inst_11831 = cljs.core.nth.call(null,inst_11830,0,null);var inst_11832 = cljs.core.nth.call(null,inst_11830,1,null);var state_11923__$1 = (function (){var statearr_11976 = state_11923;(statearr_11976[28] = inst_11831);
return statearr_11976;
})();if(cljs.core.truth_(inst_11832))
{var statearr_11977_12046 = state_11923__$1;(statearr_11977_12046[1] = 22);
} else
{var statearr_11978_12047 = state_11923__$1;(statearr_11978_12047[1] = 23);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 21))
{var inst_11841 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11979_12048 = state_11923__$1;(statearr_11979_12048[2] = inst_11841);
(statearr_11979_12048[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 22))
{var inst_11831 = (state_11923[28]);var inst_11834 = cljs.core.async.close_BANG_.call(null,inst_11831);var state_11923__$1 = state_11923;var statearr_11980_12049 = state_11923__$1;(statearr_11980_12049[2] = inst_11834);
(statearr_11980_12049[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 23))
{var state_11923__$1 = state_11923;var statearr_11981_12050 = state_11923__$1;(statearr_11981_12050[2] = null);
(statearr_11981_12050[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 24))
{var inst_11820 = (state_11923[25]);var inst_11837 = (state_11923[2]);var inst_11838 = cljs.core.next.call(null,inst_11820);var inst_11800 = inst_11838;var inst_11801 = null;var inst_11802 = 0;var inst_11803 = 0;var state_11923__$1 = (function (){var statearr_11982 = state_11923;(statearr_11982[29] = inst_11837);
(statearr_11982[14] = inst_11800);
(statearr_11982[15] = inst_11802);
(statearr_11982[16] = inst_11801);
(statearr_11982[17] = inst_11803);
return statearr_11982;
})();var statearr_11983_12051 = state_11923__$1;(statearr_11983_12051[2] = null);
(statearr_11983_12051[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 25))
{var inst_11862 = (state_11923[10]);var inst_11861 = (state_11923[11]);var inst_11864 = (inst_11862 < inst_11861);var inst_11865 = inst_11864;var state_11923__$1 = state_11923;if(cljs.core.truth_(inst_11865))
{var statearr_11984_12052 = state_11923__$1;(statearr_11984_12052[1] = 27);
} else
{var statearr_11985_12053 = state_11923__$1;(statearr_11985_12053[1] = 28);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 26))
{var inst_11851 = (state_11923[19]);var inst_11909 = (state_11923[2]);var inst_11910 = cljs.core.seq.call(null,inst_11851);var state_11923__$1 = (function (){var statearr_11986 = state_11923;(statearr_11986[30] = inst_11909);
return statearr_11986;
})();if(inst_11910)
{var statearr_11987_12054 = state_11923__$1;(statearr_11987_12054[1] = 42);
} else
{var statearr_11988_12055 = state_11923__$1;(statearr_11988_12055[1] = 43);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 27))
{var inst_11862 = (state_11923[10]);var inst_11860 = (state_11923[12]);var inst_11867 = cljs.core._nth.call(null,inst_11860,inst_11862);var state_11923__$1 = (function (){var statearr_11989 = state_11923;(statearr_11989[7] = inst_11867);
return statearr_11989;
})();var statearr_11990_12056 = state_11923__$1;(statearr_11990_12056[2] = null);
(statearr_11990_12056[1] = 32);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 28))
{var inst_11880 = (state_11923[9]);var inst_11859 = (state_11923[13]);var inst_11880__$1 = cljs.core.seq.call(null,inst_11859);var state_11923__$1 = (function (){var statearr_11994 = state_11923;(statearr_11994[9] = inst_11880__$1);
return statearr_11994;
})();if(inst_11880__$1)
{var statearr_11995_12057 = state_11923__$1;(statearr_11995_12057[1] = 33);
} else
{var statearr_11996_12058 = state_11923__$1;(statearr_11996_12058[1] = 34);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 29))
{var inst_11907 = (state_11923[2]);var state_11923__$1 = state_11923;var statearr_11997_12059 = state_11923__$1;(statearr_11997_12059[2] = inst_11907);
(statearr_11997_12059[1] = 26);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 30))
{var inst_11862 = (state_11923[10]);var inst_11861 = (state_11923[11]);var inst_11860 = (state_11923[12]);var inst_11859 = (state_11923[13]);var inst_11876 = (state_11923[2]);var inst_11877 = (inst_11862 + 1);var tmp11991 = inst_11861;var tmp11992 = inst_11860;var tmp11993 = inst_11859;var inst_11859__$1 = tmp11993;var inst_11860__$1 = tmp11992;var inst_11861__$1 = tmp11991;var inst_11862__$1 = inst_11877;var state_11923__$1 = (function (){var statearr_11998 = state_11923;(statearr_11998[10] = inst_11862__$1);
(statearr_11998[11] = inst_11861__$1);
(statearr_11998[12] = inst_11860__$1);
(statearr_11998[31] = inst_11876);
(statearr_11998[13] = inst_11859__$1);
return statearr_11998;
})();var statearr_11999_12060 = state_11923__$1;(statearr_11999_12060[2] = null);
(statearr_11999_12060[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11924 === 31))
{var inst_11867 = (state_11923[7]);var inst_11868 = (state_11923[2]);var inst_11869 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_11870 = cljs.core.async.untap_STAR_.call(null,m,inst_11867);var state_11923__$1 = (function (){var statearr_12000 = state_11923;(statearr_12000[32] = inst_11868);
(statearr_12000[33] = inst_11869);
return statearr_12000;
})();var statearr_12001_12061 = state_11923__$1;(statearr_12001_12061[2] = inst_11870);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11923__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12009,cs,m,dchan,dctr,done))
;return ((function (switch__6331__auto__,c__6346__auto___12009,cs,m,dchan,dctr,done){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12005 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_12005[0] = state_machine__6332__auto__);
(statearr_12005[1] = 1);
return statearr_12005;
});
var state_machine__6332__auto____1 = (function (state_11923){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_11923);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12006){if((e12006 instanceof Object))
{var ex__6335__auto__ = e12006;var statearr_12007_12062 = state_11923;(statearr_12007_12062[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11923);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12006;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12063 = state_11923;
state_11923 = G__12063;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_11923){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_11923);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12009,cs,m,dchan,dctr,done))
})();var state__6348__auto__ = (function (){var statearr_12008 = f__6347__auto__.call(null);(statearr_12008[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12009);
return statearr_12008;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12009,cs,m,dchan,dctr,done))
);
return m;
});
/**
* Copies the mult source onto the supplied channel.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.tap = (function() {
var tap = null;
var tap__2 = (function (mult,ch){return tap.call(null,mult,ch,true);
});
var tap__3 = (function (mult,ch,close_QMARK_){cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);
return ch;
});
tap = function(mult,ch,close_QMARK_){
switch(arguments.length){
case 2:
return tap__2.call(this,mult,ch);
case 3:
return tap__3.call(this,mult,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
tap.cljs$core$IFn$_invoke$arity$2 = tap__2;
tap.cljs$core$IFn$_invoke$arity$3 = tap__3;
return tap;
})()
;
/**
* Disconnects a target channel from a mult
*/
cljs.core.async.untap = (function untap(mult,ch){return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
* Disconnects all target channels from a mult
*/
cljs.core.async.untap_all = (function untap_all(mult){return cljs.core.async.untap_all_STAR_.call(null,mult);
});
cljs.core.async.Mix = (function (){var obj12065 = {};return obj12065;
})();
cljs.core.async.admix_STAR_ = (function admix_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$admix_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.admix_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_STAR_ = (function unmix_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_all_STAR_ = (function unmix_all_STAR_(m){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
})().call(null,m);
}
});
cljs.core.async.toggle_STAR_ = (function toggle_STAR_(m,state_map){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
})().call(null,m,state_map);
}
});
cljs.core.async.solo_mode_STAR_ = (function solo_mode_STAR_(m,mode){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
})().call(null,m,mode);
}
});
/**
* Creates and returns a mix of one or more input channels which will
* be put on the supplied out channel. Input sources can be added to
* the mix with 'admix', and removed with 'unmix'. A mix supports
* soloing, muting and pausing multiple inputs atomically using
* 'toggle', and can solo using either muting or pausing as determined
* by 'solo-mode'.
* 
* Each channel can have zero or more boolean modes set via 'toggle':
* 
* :solo - when true, only this (ond other soloed) channel(s) will appear
* in the mix output channel. :mute and :pause states of soloed
* channels are ignored. If solo-mode is :mute, non-soloed
* channels are muted, if :pause, non-soloed channels are
* paused.
* 
* :mute - muted channels will have their contents consumed but not included in the mix
* :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
*/
cljs.core.async.mix = (function mix(out){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",1120344424),null,new cljs.core.Keyword(null,"mute","mute",1017267595),null], null), null);var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",1017440337));var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595));var change = cljs.core.async.chan.call(null);var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){if(cljs.core.truth_(attr.call(null,v)))
{return cljs.core.conj.call(null,ret,c);
} else
{return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){var chs = cljs.core.deref.call(null,cs);var mode = cljs.core.deref.call(null,solo_mode);var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",1017440337),chs);var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",1120344424),chs);return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1123523302),solos,new cljs.core.Keyword(null,"mutes","mutes",1118168300),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595),chs),new cljs.core.Keyword(null,"reads","reads",1122290959),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",1120344424))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;var m = (function (){if(typeof cljs.core.async.t12175 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t12175 = (function (pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,meta12176){
this.pick = pick;
this.out = out;
this.attrs = attrs;
this.cs = cs;
this.calc_state = calc_state;
this.solo_modes = solo_modes;
this.mix = mix;
this.changed = changed;
this.change = change;
this.solo_mode = solo_mode;
this.meta12176 = meta12176;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t12175.cljs$lang$type = true;
cljs.core.async.t12175.cljs$lang$ctorStr = "cljs.core.async/t12175";
cljs.core.async.t12175.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t12175");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$ = true;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.solo_modes.call(null,mode)))
{} else
{throw (new Error(("Assert failed: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(("mode must be one of: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)))+"\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",-1162732933,null),new cljs.core.Symbol(null,"mode","mode",-1637174436,null)))))));
}
cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t12175.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_12177){var self__ = this;
var _12177__$1 = this;return self__.meta12176;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t12175.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_12177,meta12176__$1){var self__ = this;
var _12177__$1 = this;return (new cljs.core.async.t12175(self__.pick,self__.out,self__.attrs,self__.cs,self__.calc_state,self__.solo_modes,self__.mix,self__.changed,self__.change,self__.solo_mode,meta12176__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.__GT_t12175 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function __GT_t12175(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta12176){return (new cljs.core.async.t12175(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta12176));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
}
return (new cljs.core.async.t12175(pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,null));
})();var c__6346__auto___12284 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_12242){var state_val_12243 = (state_12242[1]);if((state_val_12243 === 1))
{var inst_12181 = (state_12242[7]);var inst_12181__$1 = calc_state.call(null);var inst_12182 = cljs.core.seq_QMARK_.call(null,inst_12181__$1);var state_12242__$1 = (function (){var statearr_12244 = state_12242;(statearr_12244[7] = inst_12181__$1);
return statearr_12244;
})();if(inst_12182)
{var statearr_12245_12285 = state_12242__$1;(statearr_12245_12285[1] = 2);
} else
{var statearr_12246_12286 = state_12242__$1;(statearr_12246_12286[1] = 3);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 2))
{var inst_12181 = (state_12242[7]);var inst_12184 = cljs.core.apply.call(null,cljs.core.hash_map,inst_12181);var state_12242__$1 = state_12242;var statearr_12247_12287 = state_12242__$1;(statearr_12247_12287[2] = inst_12184);
(statearr_12247_12287[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 3))
{var inst_12181 = (state_12242[7]);var state_12242__$1 = state_12242;var statearr_12248_12288 = state_12242__$1;(statearr_12248_12288[2] = inst_12181);
(statearr_12248_12288[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 4))
{var inst_12181 = (state_12242[7]);var inst_12187 = (state_12242[2]);var inst_12188 = cljs.core.get.call(null,inst_12187,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_12189 = cljs.core.get.call(null,inst_12187,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_12190 = cljs.core.get.call(null,inst_12187,new cljs.core.Keyword(null,"solos","solos",1123523302));var inst_12191 = inst_12181;var state_12242__$1 = (function (){var statearr_12249 = state_12242;(statearr_12249[8] = inst_12188);
(statearr_12249[9] = inst_12189);
(statearr_12249[10] = inst_12190);
(statearr_12249[11] = inst_12191);
return statearr_12249;
})();var statearr_12250_12289 = state_12242__$1;(statearr_12250_12289[2] = null);
(statearr_12250_12289[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 5))
{var inst_12191 = (state_12242[11]);var inst_12194 = cljs.core.seq_QMARK_.call(null,inst_12191);var state_12242__$1 = state_12242;if(inst_12194)
{var statearr_12251_12290 = state_12242__$1;(statearr_12251_12290[1] = 7);
} else
{var statearr_12252_12291 = state_12242__$1;(statearr_12252_12291[1] = 8);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 6))
{var inst_12240 = (state_12242[2]);var state_12242__$1 = state_12242;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_12242__$1,inst_12240);
} else
{if((state_val_12243 === 7))
{var inst_12191 = (state_12242[11]);var inst_12196 = cljs.core.apply.call(null,cljs.core.hash_map,inst_12191);var state_12242__$1 = state_12242;var statearr_12253_12292 = state_12242__$1;(statearr_12253_12292[2] = inst_12196);
(statearr_12253_12292[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 8))
{var inst_12191 = (state_12242[11]);var state_12242__$1 = state_12242;var statearr_12254_12293 = state_12242__$1;(statearr_12254_12293[2] = inst_12191);
(statearr_12254_12293[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 9))
{var inst_12199 = (state_12242[12]);var inst_12199__$1 = (state_12242[2]);var inst_12200 = cljs.core.get.call(null,inst_12199__$1,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_12201 = cljs.core.get.call(null,inst_12199__$1,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_12202 = cljs.core.get.call(null,inst_12199__$1,new cljs.core.Keyword(null,"solos","solos",1123523302));var state_12242__$1 = (function (){var statearr_12255 = state_12242;(statearr_12255[12] = inst_12199__$1);
(statearr_12255[13] = inst_12202);
(statearr_12255[14] = inst_12201);
return statearr_12255;
})();return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_12242__$1,10,inst_12200);
} else
{if((state_val_12243 === 10))
{var inst_12206 = (state_12242[15]);var inst_12207 = (state_12242[16]);var inst_12205 = (state_12242[2]);var inst_12206__$1 = cljs.core.nth.call(null,inst_12205,0,null);var inst_12207__$1 = cljs.core.nth.call(null,inst_12205,1,null);var inst_12208 = (inst_12206__$1 == null);var inst_12209 = cljs.core._EQ_.call(null,inst_12207__$1,change);var inst_12210 = (inst_12208) || (inst_12209);var state_12242__$1 = (function (){var statearr_12256 = state_12242;(statearr_12256[15] = inst_12206__$1);
(statearr_12256[16] = inst_12207__$1);
return statearr_12256;
})();if(cljs.core.truth_(inst_12210))
{var statearr_12257_12294 = state_12242__$1;(statearr_12257_12294[1] = 11);
} else
{var statearr_12258_12295 = state_12242__$1;(statearr_12258_12295[1] = 12);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 11))
{var inst_12206 = (state_12242[15]);var inst_12212 = (inst_12206 == null);var state_12242__$1 = state_12242;if(cljs.core.truth_(inst_12212))
{var statearr_12259_12296 = state_12242__$1;(statearr_12259_12296[1] = 14);
} else
{var statearr_12260_12297 = state_12242__$1;(statearr_12260_12297[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 12))
{var inst_12221 = (state_12242[17]);var inst_12207 = (state_12242[16]);var inst_12202 = (state_12242[13]);var inst_12221__$1 = inst_12202.call(null,inst_12207);var state_12242__$1 = (function (){var statearr_12261 = state_12242;(statearr_12261[17] = inst_12221__$1);
return statearr_12261;
})();if(cljs.core.truth_(inst_12221__$1))
{var statearr_12262_12298 = state_12242__$1;(statearr_12262_12298[1] = 17);
} else
{var statearr_12263_12299 = state_12242__$1;(statearr_12263_12299[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 13))
{var inst_12238 = (state_12242[2]);var state_12242__$1 = state_12242;var statearr_12264_12300 = state_12242__$1;(statearr_12264_12300[2] = inst_12238);
(statearr_12264_12300[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 14))
{var inst_12207 = (state_12242[16]);var inst_12214 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_12207);var state_12242__$1 = state_12242;var statearr_12265_12301 = state_12242__$1;(statearr_12265_12301[2] = inst_12214);
(statearr_12265_12301[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 15))
{var state_12242__$1 = state_12242;var statearr_12266_12302 = state_12242__$1;(statearr_12266_12302[2] = null);
(statearr_12266_12302[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 16))
{var inst_12217 = (state_12242[2]);var inst_12218 = calc_state.call(null);var inst_12191 = inst_12218;var state_12242__$1 = (function (){var statearr_12267 = state_12242;(statearr_12267[18] = inst_12217);
(statearr_12267[11] = inst_12191);
return statearr_12267;
})();var statearr_12268_12303 = state_12242__$1;(statearr_12268_12303[2] = null);
(statearr_12268_12303[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 17))
{var inst_12221 = (state_12242[17]);var state_12242__$1 = state_12242;var statearr_12269_12304 = state_12242__$1;(statearr_12269_12304[2] = inst_12221);
(statearr_12269_12304[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 18))
{var inst_12207 = (state_12242[16]);var inst_12202 = (state_12242[13]);var inst_12201 = (state_12242[14]);var inst_12224 = cljs.core.empty_QMARK_.call(null,inst_12202);var inst_12225 = inst_12201.call(null,inst_12207);var inst_12226 = cljs.core.not.call(null,inst_12225);var inst_12227 = (inst_12224) && (inst_12226);var state_12242__$1 = state_12242;var statearr_12270_12305 = state_12242__$1;(statearr_12270_12305[2] = inst_12227);
(statearr_12270_12305[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 19))
{var inst_12229 = (state_12242[2]);var state_12242__$1 = state_12242;if(cljs.core.truth_(inst_12229))
{var statearr_12271_12306 = state_12242__$1;(statearr_12271_12306[1] = 20);
} else
{var statearr_12272_12307 = state_12242__$1;(statearr_12272_12307[1] = 21);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 20))
{var inst_12206 = (state_12242[15]);var state_12242__$1 = state_12242;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_12242__$1,23,out,inst_12206);
} else
{if((state_val_12243 === 21))
{var state_12242__$1 = state_12242;var statearr_12273_12308 = state_12242__$1;(statearr_12273_12308[2] = null);
(statearr_12273_12308[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 22))
{var inst_12199 = (state_12242[12]);var inst_12235 = (state_12242[2]);var inst_12191 = inst_12199;var state_12242__$1 = (function (){var statearr_12274 = state_12242;(statearr_12274[19] = inst_12235);
(statearr_12274[11] = inst_12191);
return statearr_12274;
})();var statearr_12275_12309 = state_12242__$1;(statearr_12275_12309[2] = null);
(statearr_12275_12309[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12243 === 23))
{var inst_12232 = (state_12242[2]);var state_12242__$1 = state_12242;var statearr_12276_12310 = state_12242__$1;(statearr_12276_12310[2] = inst_12232);
(statearr_12276_12310[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;return ((function (switch__6331__auto__,c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12280 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_12280[0] = state_machine__6332__auto__);
(statearr_12280[1] = 1);
return statearr_12280;
});
var state_machine__6332__auto____1 = (function (state_12242){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_12242);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12281){if((e12281 instanceof Object))
{var ex__6335__auto__ = e12281;var statearr_12282_12311 = state_12242;(statearr_12282_12311[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12242);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12281;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12312 = state_12242;
state_12242 = G__12312;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_12242){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_12242);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();var state__6348__auto__ = (function (){var statearr_12283 = f__6347__auto__.call(null);(statearr_12283[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12284);
return statearr_12283;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12284,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);
return m;
});
/**
* Adds ch as an input to the mix
*/
cljs.core.async.admix = (function admix(mix,ch){return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
* Removes ch as an input to the mix
*/
cljs.core.async.unmix = (function unmix(mix,ch){return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
* removes all inputs from the mix
*/
cljs.core.async.unmix_all = (function unmix_all(mix){return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
* Atomically sets the state(s) of one or more channels in a mix. The
* state map is a map of channels -> channel-state-map. A
* channel-state-map is a map of attrs -> boolean, where attr is one or
* more of :mute, :pause or :solo. Any states supplied are merged with
* the current state.
* 
* Note that channels can be added to a mix via toggle, which can be
* used to add channels in a particular (e.g. paused) state.
*/
cljs.core.async.toggle = (function toggle(mix,state_map){return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
* Sets the solo mode of the mix. mode must be one of :mute or :pause
*/
cljs.core.async.solo_mode = (function solo_mode(mix,mode){return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});
cljs.core.async.Pub = (function (){var obj12314 = {};return obj12314;
})();
cljs.core.async.sub_STAR_ = (function sub_STAR_(p,v,ch,close_QMARK_){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$sub_STAR_$arity$4;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.sub_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
})().call(null,p,v,ch,close_QMARK_);
}
});
cljs.core.async.unsub_STAR_ = (function unsub_STAR_(p,v,ch){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
})().call(null,p,v,ch);
}
});
cljs.core.async.unsub_all_STAR_ = (function() {
var unsub_all_STAR_ = null;
var unsub_all_STAR___1 = (function (p){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p);
}
});
var unsub_all_STAR___2 = (function (p,v){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p,v);
}
});
unsub_all_STAR_ = function(p,v){
switch(arguments.length){
case 1:
return unsub_all_STAR___1.call(this,p);
case 2:
return unsub_all_STAR___2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = unsub_all_STAR___1;
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = unsub_all_STAR___2;
return unsub_all_STAR_;
})()
;
/**
* Creates and returns a pub(lication) of the supplied channel,
* partitioned into topics by the topic-fn. topic-fn will be applied to
* each value on the channel and the result will determine the 'topic'
* on which that value will be put. Channels can be subscribed to
* receive copies of topics using 'sub', and unsubscribed using
* 'unsub'. Each topic will be handled by an internal mult on a
* dedicated channel. By default these internal channels are
* unbuffered, but a buf-fn can be supplied which, given a topic,
* creates a buffer with desired properties.
* 
* Each item is distributed to all subs in parallel and synchronously,
* i.e. each sub must accept before the next item is distributed. Use
* buffering/windowing to prevent slow subs from holding up the pub.
* 
* Items received when there are no matching subs get dropped.
* 
* Note that if buf-fns are used then each topic is handled
* asynchronously, i.e. if a channel is subscribed to more than one
* topic it should not expect them to be interleaved identically with
* the source.
*/
cljs.core.async.pub = (function() {
var pub = null;
var pub__2 = (function (ch,topic_fn){return pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});
var pub__3 = (function (ch,topic_fn,buf_fn){var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var ensure_mult = ((function (mults){
return (function (topic){var or__3538__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__3538__auto__,mults){
return (function (p1__12315_SHARP_){if(cljs.core.truth_(p1__12315_SHARP_.call(null,topic)))
{return p1__12315_SHARP_;
} else
{return cljs.core.assoc.call(null,p1__12315_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__3538__auto__,mults))
),topic);
}
});})(mults))
;var p = (function (){if(typeof cljs.core.async.t12440 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t12440 = (function (ensure_mult,mults,buf_fn,topic_fn,ch,pub,meta12441){
this.ensure_mult = ensure_mult;
this.mults = mults;
this.buf_fn = buf_fn;
this.topic_fn = topic_fn;
this.ch = ch;
this.pub = pub;
this.meta12441 = meta12441;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t12440.cljs$lang$type = true;
cljs.core.async.t12440.cljs$lang$ctorStr = "cljs.core.async/t12440";
cljs.core.async.t12440.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t12440");
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$async$Pub$ = true;
cljs.core.async.t12440.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2,close_QMARK_){var self__ = this;
var p__$1 = this;var m = self__.ensure_mult.call(null,topic);return cljs.core.async.tap.call(null,m,ch__$2,close_QMARK_);
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2){var self__ = this;
var p__$1 = this;var temp__4092__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);if(cljs.core.truth_(temp__4092__auto__))
{var m = temp__4092__auto__;return cljs.core.async.untap.call(null,m,ch__$2);
} else
{return null;
}
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){var self__ = this;
var ___$1 = this;return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t12440.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_12442){var self__ = this;
var _12442__$1 = this;return self__.meta12441;
});})(mults,ensure_mult))
;
cljs.core.async.t12440.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_12442,meta12441__$1){var self__ = this;
var _12442__$1 = this;return (new cljs.core.async.t12440(self__.ensure_mult,self__.mults,self__.buf_fn,self__.topic_fn,self__.ch,self__.pub,meta12441__$1));
});})(mults,ensure_mult))
;
cljs.core.async.__GT_t12440 = ((function (mults,ensure_mult){
return (function __GT_t12440(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta12441){return (new cljs.core.async.t12440(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta12441));
});})(mults,ensure_mult))
;
}
return (new cljs.core.async.t12440(ensure_mult,mults,buf_fn,topic_fn,ch,pub,null));
})();var c__6346__auto___12564 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12564,mults,ensure_mult,p){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12564,mults,ensure_mult,p){
return (function (state_12516){var state_val_12517 = (state_12516[1]);if((state_val_12517 === 1))
{var state_12516__$1 = state_12516;var statearr_12518_12565 = state_12516__$1;(statearr_12518_12565[2] = null);
(statearr_12518_12565[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 2))
{var state_12516__$1 = state_12516;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_12516__$1,4,ch);
} else
{if((state_val_12517 === 3))
{var inst_12514 = (state_12516[2]);var state_12516__$1 = state_12516;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_12516__$1,inst_12514);
} else
{if((state_val_12517 === 4))
{var inst_12445 = (state_12516[7]);var inst_12445__$1 = (state_12516[2]);var inst_12446 = (inst_12445__$1 == null);var state_12516__$1 = (function (){var statearr_12519 = state_12516;(statearr_12519[7] = inst_12445__$1);
return statearr_12519;
})();if(cljs.core.truth_(inst_12446))
{var statearr_12520_12566 = state_12516__$1;(statearr_12520_12566[1] = 5);
} else
{var statearr_12521_12567 = state_12516__$1;(statearr_12521_12567[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 5))
{var inst_12452 = cljs.core.deref.call(null,mults);var inst_12453 = cljs.core.vals.call(null,inst_12452);var inst_12454 = cljs.core.seq.call(null,inst_12453);var inst_12455 = inst_12454;var inst_12456 = null;var inst_12457 = 0;var inst_12458 = 0;var state_12516__$1 = (function (){var statearr_12522 = state_12516;(statearr_12522[8] = inst_12457);
(statearr_12522[9] = inst_12458);
(statearr_12522[10] = inst_12456);
(statearr_12522[11] = inst_12455);
return statearr_12522;
})();var statearr_12523_12568 = state_12516__$1;(statearr_12523_12568[2] = null);
(statearr_12523_12568[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 6))
{var inst_12495 = (state_12516[12]);var inst_12493 = (state_12516[13]);var inst_12445 = (state_12516[7]);var inst_12493__$1 = topic_fn.call(null,inst_12445);var inst_12494 = cljs.core.deref.call(null,mults);var inst_12495__$1 = cljs.core.get.call(null,inst_12494,inst_12493__$1);var state_12516__$1 = (function (){var statearr_12524 = state_12516;(statearr_12524[12] = inst_12495__$1);
(statearr_12524[13] = inst_12493__$1);
return statearr_12524;
})();if(cljs.core.truth_(inst_12495__$1))
{var statearr_12525_12569 = state_12516__$1;(statearr_12525_12569[1] = 19);
} else
{var statearr_12526_12570 = state_12516__$1;(statearr_12526_12570[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 7))
{var inst_12512 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12527_12571 = state_12516__$1;(statearr_12527_12571[2] = inst_12512);
(statearr_12527_12571[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 8))
{var inst_12457 = (state_12516[8]);var inst_12458 = (state_12516[9]);var inst_12460 = (inst_12458 < inst_12457);var inst_12461 = inst_12460;var state_12516__$1 = state_12516;if(cljs.core.truth_(inst_12461))
{var statearr_12531_12572 = state_12516__$1;(statearr_12531_12572[1] = 10);
} else
{var statearr_12532_12573 = state_12516__$1;(statearr_12532_12573[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 9))
{var inst_12491 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12533_12574 = state_12516__$1;(statearr_12533_12574[2] = inst_12491);
(statearr_12533_12574[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 10))
{var inst_12457 = (state_12516[8]);var inst_12458 = (state_12516[9]);var inst_12456 = (state_12516[10]);var inst_12455 = (state_12516[11]);var inst_12463 = cljs.core._nth.call(null,inst_12456,inst_12458);var inst_12464 = cljs.core.async.muxch_STAR_.call(null,inst_12463);var inst_12465 = cljs.core.async.close_BANG_.call(null,inst_12464);var inst_12466 = (inst_12458 + 1);var tmp12528 = inst_12457;var tmp12529 = inst_12456;var tmp12530 = inst_12455;var inst_12455__$1 = tmp12530;var inst_12456__$1 = tmp12529;var inst_12457__$1 = tmp12528;var inst_12458__$1 = inst_12466;var state_12516__$1 = (function (){var statearr_12534 = state_12516;(statearr_12534[8] = inst_12457__$1);
(statearr_12534[9] = inst_12458__$1);
(statearr_12534[14] = inst_12465);
(statearr_12534[10] = inst_12456__$1);
(statearr_12534[11] = inst_12455__$1);
return statearr_12534;
})();var statearr_12535_12575 = state_12516__$1;(statearr_12535_12575[2] = null);
(statearr_12535_12575[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 11))
{var inst_12469 = (state_12516[15]);var inst_12455 = (state_12516[11]);var inst_12469__$1 = cljs.core.seq.call(null,inst_12455);var state_12516__$1 = (function (){var statearr_12536 = state_12516;(statearr_12536[15] = inst_12469__$1);
return statearr_12536;
})();if(inst_12469__$1)
{var statearr_12537_12576 = state_12516__$1;(statearr_12537_12576[1] = 13);
} else
{var statearr_12538_12577 = state_12516__$1;(statearr_12538_12577[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 12))
{var inst_12489 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12539_12578 = state_12516__$1;(statearr_12539_12578[2] = inst_12489);
(statearr_12539_12578[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 13))
{var inst_12469 = (state_12516[15]);var inst_12471 = cljs.core.chunked_seq_QMARK_.call(null,inst_12469);var state_12516__$1 = state_12516;if(inst_12471)
{var statearr_12540_12579 = state_12516__$1;(statearr_12540_12579[1] = 16);
} else
{var statearr_12541_12580 = state_12516__$1;(statearr_12541_12580[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 14))
{var state_12516__$1 = state_12516;var statearr_12542_12581 = state_12516__$1;(statearr_12542_12581[2] = null);
(statearr_12542_12581[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 15))
{var inst_12487 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12543_12582 = state_12516__$1;(statearr_12543_12582[2] = inst_12487);
(statearr_12543_12582[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 16))
{var inst_12469 = (state_12516[15]);var inst_12473 = cljs.core.chunk_first.call(null,inst_12469);var inst_12474 = cljs.core.chunk_rest.call(null,inst_12469);var inst_12475 = cljs.core.count.call(null,inst_12473);var inst_12455 = inst_12474;var inst_12456 = inst_12473;var inst_12457 = inst_12475;var inst_12458 = 0;var state_12516__$1 = (function (){var statearr_12544 = state_12516;(statearr_12544[8] = inst_12457);
(statearr_12544[9] = inst_12458);
(statearr_12544[10] = inst_12456);
(statearr_12544[11] = inst_12455);
return statearr_12544;
})();var statearr_12545_12583 = state_12516__$1;(statearr_12545_12583[2] = null);
(statearr_12545_12583[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 17))
{var inst_12469 = (state_12516[15]);var inst_12478 = cljs.core.first.call(null,inst_12469);var inst_12479 = cljs.core.async.muxch_STAR_.call(null,inst_12478);var inst_12480 = cljs.core.async.close_BANG_.call(null,inst_12479);var inst_12481 = cljs.core.next.call(null,inst_12469);var inst_12455 = inst_12481;var inst_12456 = null;var inst_12457 = 0;var inst_12458 = 0;var state_12516__$1 = (function (){var statearr_12546 = state_12516;(statearr_12546[8] = inst_12457);
(statearr_12546[9] = inst_12458);
(statearr_12546[10] = inst_12456);
(statearr_12546[11] = inst_12455);
(statearr_12546[16] = inst_12480);
return statearr_12546;
})();var statearr_12547_12584 = state_12516__$1;(statearr_12547_12584[2] = null);
(statearr_12547_12584[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 18))
{var inst_12484 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12548_12585 = state_12516__$1;(statearr_12548_12585[2] = inst_12484);
(statearr_12548_12585[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 19))
{var state_12516__$1 = state_12516;var statearr_12549_12586 = state_12516__$1;(statearr_12549_12586[2] = null);
(statearr_12549_12586[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 20))
{var state_12516__$1 = state_12516;var statearr_12550_12587 = state_12516__$1;(statearr_12550_12587[2] = null);
(statearr_12550_12587[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 21))
{var inst_12509 = (state_12516[2]);var state_12516__$1 = (function (){var statearr_12551 = state_12516;(statearr_12551[17] = inst_12509);
return statearr_12551;
})();var statearr_12552_12588 = state_12516__$1;(statearr_12552_12588[2] = null);
(statearr_12552_12588[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 22))
{var inst_12506 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12553_12589 = state_12516__$1;(statearr_12553_12589[2] = inst_12506);
(statearr_12553_12589[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 23))
{var inst_12493 = (state_12516[13]);var inst_12497 = (state_12516[2]);var inst_12498 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_12493);var state_12516__$1 = (function (){var statearr_12554 = state_12516;(statearr_12554[18] = inst_12497);
return statearr_12554;
})();var statearr_12555_12590 = state_12516__$1;(statearr_12555_12590[2] = inst_12498);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12516__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12517 === 24))
{var inst_12495 = (state_12516[12]);var inst_12445 = (state_12516[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_12516,23,Object,null,22);var inst_12502 = cljs.core.async.muxch_STAR_.call(null,inst_12495);var state_12516__$1 = state_12516;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_12516__$1,25,inst_12502,inst_12445);
} else
{if((state_val_12517 === 25))
{var inst_12504 = (state_12516[2]);var state_12516__$1 = state_12516;var statearr_12556_12591 = state_12516__$1;(statearr_12556_12591[2] = inst_12504);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12516__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12564,mults,ensure_mult,p))
;return ((function (switch__6331__auto__,c__6346__auto___12564,mults,ensure_mult,p){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12560 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_12560[0] = state_machine__6332__auto__);
(statearr_12560[1] = 1);
return statearr_12560;
});
var state_machine__6332__auto____1 = (function (state_12516){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_12516);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12561){if((e12561 instanceof Object))
{var ex__6335__auto__ = e12561;var statearr_12562_12592 = state_12516;(statearr_12562_12592[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12516);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12561;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12593 = state_12516;
state_12516 = G__12593;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_12516){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_12516);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12564,mults,ensure_mult,p))
})();var state__6348__auto__ = (function (){var statearr_12563 = f__6347__auto__.call(null);(statearr_12563[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12564);
return statearr_12563;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12564,mults,ensure_mult,p))
);
return p;
});
pub = function(ch,topic_fn,buf_fn){
switch(arguments.length){
case 2:
return pub__2.call(this,ch,topic_fn);
case 3:
return pub__3.call(this,ch,topic_fn,buf_fn);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pub.cljs$core$IFn$_invoke$arity$2 = pub__2;
pub.cljs$core$IFn$_invoke$arity$3 = pub__3;
return pub;
})()
;
/**
* Subscribes a channel to a topic of a pub.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.sub = (function() {
var sub = null;
var sub__3 = (function (p,topic,ch){return sub.call(null,p,topic,ch,true);
});
var sub__4 = (function (p,topic,ch,close_QMARK_){return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});
sub = function(p,topic,ch,close_QMARK_){
switch(arguments.length){
case 3:
return sub__3.call(this,p,topic,ch);
case 4:
return sub__4.call(this,p,topic,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
sub.cljs$core$IFn$_invoke$arity$3 = sub__3;
sub.cljs$core$IFn$_invoke$arity$4 = sub__4;
return sub;
})()
;
/**
* Unsubscribes a channel from a topic of a pub
*/
cljs.core.async.unsub = (function unsub(p,topic,ch){return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
* Unsubscribes all channels from a pub, or a topic of a pub
*/
cljs.core.async.unsub_all = (function() {
var unsub_all = null;
var unsub_all__1 = (function (p){return cljs.core.async.unsub_all_STAR_.call(null,p);
});
var unsub_all__2 = (function (p,topic){return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});
unsub_all = function(p,topic){
switch(arguments.length){
case 1:
return unsub_all__1.call(this,p);
case 2:
return unsub_all__2.call(this,p,topic);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all.cljs$core$IFn$_invoke$arity$1 = unsub_all__1;
unsub_all.cljs$core$IFn$_invoke$arity$2 = unsub_all__2;
return unsub_all;
})()
;
/**
* Takes a function and a collection of source channels, and returns a
* channel which contains the values produced by applying f to the set
* of first items taken from each source channel, followed by applying
* f to the set of second items from each channel, until any one of the
* channels is closed, at which point the output channel will be
* closed. The returned channel will be unbuffered by default, or a
* buf-or-n can be supplied
*/
cljs.core.async.map = (function() {
var map = null;
var map__2 = (function (f,chs){return map.call(null,f,chs,null);
});
var map__3 = (function (f,chs,buf_or_n){var chs__$1 = cljs.core.vec.call(null,chs);var out = cljs.core.async.chan.call(null,buf_or_n);var cnt = cljs.core.count.call(null,chs__$1);var rets = cljs.core.object_array.call(null,cnt);var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){(rets[i] = ret);
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,rets.slice(0));
} else
{return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));var c__6346__auto___12730 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_12700){var state_val_12701 = (state_12700[1]);if((state_val_12701 === 1))
{var state_12700__$1 = state_12700;var statearr_12702_12731 = state_12700__$1;(statearr_12702_12731[2] = null);
(statearr_12702_12731[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 2))
{var inst_12663 = cljs.core.reset_BANG_.call(null,dctr,cnt);var inst_12664 = 0;var state_12700__$1 = (function (){var statearr_12703 = state_12700;(statearr_12703[7] = inst_12664);
(statearr_12703[8] = inst_12663);
return statearr_12703;
})();var statearr_12704_12732 = state_12700__$1;(statearr_12704_12732[2] = null);
(statearr_12704_12732[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 3))
{var inst_12698 = (state_12700[2]);var state_12700__$1 = state_12700;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_12700__$1,inst_12698);
} else
{if((state_val_12701 === 4))
{var inst_12664 = (state_12700[7]);var inst_12666 = (inst_12664 < cnt);var state_12700__$1 = state_12700;if(cljs.core.truth_(inst_12666))
{var statearr_12705_12733 = state_12700__$1;(statearr_12705_12733[1] = 6);
} else
{var statearr_12706_12734 = state_12700__$1;(statearr_12706_12734[1] = 7);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 5))
{var inst_12684 = (state_12700[2]);var state_12700__$1 = (function (){var statearr_12707 = state_12700;(statearr_12707[9] = inst_12684);
return statearr_12707;
})();return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_12700__$1,12,dchan);
} else
{if((state_val_12701 === 6))
{var state_12700__$1 = state_12700;var statearr_12708_12735 = state_12700__$1;(statearr_12708_12735[2] = null);
(statearr_12708_12735[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 7))
{var state_12700__$1 = state_12700;var statearr_12709_12736 = state_12700__$1;(statearr_12709_12736[2] = null);
(statearr_12709_12736[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 8))
{var inst_12682 = (state_12700[2]);var state_12700__$1 = state_12700;var statearr_12710_12737 = state_12700__$1;(statearr_12710_12737[2] = inst_12682);
(statearr_12710_12737[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 9))
{var inst_12664 = (state_12700[7]);var inst_12677 = (state_12700[2]);var inst_12678 = (inst_12664 + 1);var inst_12664__$1 = inst_12678;var state_12700__$1 = (function (){var statearr_12711 = state_12700;(statearr_12711[7] = inst_12664__$1);
(statearr_12711[10] = inst_12677);
return statearr_12711;
})();var statearr_12712_12738 = state_12700__$1;(statearr_12712_12738[2] = null);
(statearr_12712_12738[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 10))
{var inst_12668 = (state_12700[2]);var inst_12669 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var state_12700__$1 = (function (){var statearr_12713 = state_12700;(statearr_12713[11] = inst_12668);
return statearr_12713;
})();var statearr_12714_12739 = state_12700__$1;(statearr_12714_12739[2] = inst_12669);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12700__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 11))
{var inst_12664 = (state_12700[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_12700,10,Object,null,9);var inst_12673 = chs__$1.call(null,inst_12664);var inst_12674 = done.call(null,inst_12664);var inst_12675 = cljs.core.async.take_BANG_.call(null,inst_12673,inst_12674);var state_12700__$1 = state_12700;var statearr_12715_12740 = state_12700__$1;(statearr_12715_12740[2] = inst_12675);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12700__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 12))
{var inst_12686 = (state_12700[12]);var inst_12686__$1 = (state_12700[2]);var inst_12687 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_12686__$1);var state_12700__$1 = (function (){var statearr_12716 = state_12700;(statearr_12716[12] = inst_12686__$1);
return statearr_12716;
})();if(cljs.core.truth_(inst_12687))
{var statearr_12717_12741 = state_12700__$1;(statearr_12717_12741[1] = 13);
} else
{var statearr_12718_12742 = state_12700__$1;(statearr_12718_12742[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 13))
{var inst_12689 = cljs.core.async.close_BANG_.call(null,out);var state_12700__$1 = state_12700;var statearr_12719_12743 = state_12700__$1;(statearr_12719_12743[2] = inst_12689);
(statearr_12719_12743[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 14))
{var inst_12686 = (state_12700[12]);var inst_12691 = cljs.core.apply.call(null,f,inst_12686);var state_12700__$1 = state_12700;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_12700__$1,16,out,inst_12691);
} else
{if((state_val_12701 === 15))
{var inst_12696 = (state_12700[2]);var state_12700__$1 = state_12700;var statearr_12720_12744 = state_12700__$1;(statearr_12720_12744[2] = inst_12696);
(statearr_12720_12744[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12701 === 16))
{var inst_12693 = (state_12700[2]);var state_12700__$1 = (function (){var statearr_12721 = state_12700;(statearr_12721[13] = inst_12693);
return statearr_12721;
})();var statearr_12722_12745 = state_12700__$1;(statearr_12722_12745[2] = null);
(statearr_12722_12745[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done))
;return ((function (switch__6331__auto__,c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12726 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_12726[0] = state_machine__6332__auto__);
(statearr_12726[1] = 1);
return statearr_12726;
});
var state_machine__6332__auto____1 = (function (state_12700){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_12700);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12727){if((e12727 instanceof Object))
{var ex__6335__auto__ = e12727;var statearr_12728_12746 = state_12700;(statearr_12728_12746[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12700);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12727;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12747 = state_12700;
state_12700 = G__12747;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_12700){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_12700);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done))
})();var state__6348__auto__ = (function (){var statearr_12729 = f__6347__auto__.call(null);(statearr_12729[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12730);
return statearr_12729;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12730,chs__$1,out,cnt,rets,dchan,dctr,done))
);
return out;
});
map = function(f,chs,buf_or_n){
switch(arguments.length){
case 2:
return map__2.call(this,f,chs);
case 3:
return map__3.call(this,f,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
map.cljs$core$IFn$_invoke$arity$2 = map__2;
map.cljs$core$IFn$_invoke$arity$3 = map__3;
return map;
})()
;
/**
* Takes a collection of source channels and returns a channel which
* contains all values taken from them. The returned channel will be
* unbuffered by default, or a buf-or-n can be supplied. The channel
* will close after all the source channels have closed.
*/
cljs.core.async.merge = (function() {
var merge = null;
var merge__1 = (function (chs){return merge.call(null,chs,null);
});
var merge__2 = (function (chs,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___12855 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12855,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12855,out){
return (function (state_12831){var state_val_12832 = (state_12831[1]);if((state_val_12832 === 1))
{var inst_12802 = cljs.core.vec.call(null,chs);var inst_12803 = inst_12802;var state_12831__$1 = (function (){var statearr_12833 = state_12831;(statearr_12833[7] = inst_12803);
return statearr_12833;
})();var statearr_12834_12856 = state_12831__$1;(statearr_12834_12856[2] = null);
(statearr_12834_12856[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 2))
{var inst_12803 = (state_12831[7]);var inst_12805 = cljs.core.count.call(null,inst_12803);var inst_12806 = (inst_12805 > 0);var state_12831__$1 = state_12831;if(cljs.core.truth_(inst_12806))
{var statearr_12835_12857 = state_12831__$1;(statearr_12835_12857[1] = 4);
} else
{var statearr_12836_12858 = state_12831__$1;(statearr_12836_12858[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 3))
{var inst_12829 = (state_12831[2]);var state_12831__$1 = state_12831;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_12831__$1,inst_12829);
} else
{if((state_val_12832 === 4))
{var inst_12803 = (state_12831[7]);var state_12831__$1 = state_12831;return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_12831__$1,7,inst_12803);
} else
{if((state_val_12832 === 5))
{var inst_12825 = cljs.core.async.close_BANG_.call(null,out);var state_12831__$1 = state_12831;var statearr_12837_12859 = state_12831__$1;(statearr_12837_12859[2] = inst_12825);
(statearr_12837_12859[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 6))
{var inst_12827 = (state_12831[2]);var state_12831__$1 = state_12831;var statearr_12838_12860 = state_12831__$1;(statearr_12838_12860[2] = inst_12827);
(statearr_12838_12860[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 7))
{var inst_12810 = (state_12831[8]);var inst_12811 = (state_12831[9]);var inst_12810__$1 = (state_12831[2]);var inst_12811__$1 = cljs.core.nth.call(null,inst_12810__$1,0,null);var inst_12812 = cljs.core.nth.call(null,inst_12810__$1,1,null);var inst_12813 = (inst_12811__$1 == null);var state_12831__$1 = (function (){var statearr_12839 = state_12831;(statearr_12839[8] = inst_12810__$1);
(statearr_12839[9] = inst_12811__$1);
(statearr_12839[10] = inst_12812);
return statearr_12839;
})();if(cljs.core.truth_(inst_12813))
{var statearr_12840_12861 = state_12831__$1;(statearr_12840_12861[1] = 8);
} else
{var statearr_12841_12862 = state_12831__$1;(statearr_12841_12862[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 8))
{var inst_12803 = (state_12831[7]);var inst_12810 = (state_12831[8]);var inst_12811 = (state_12831[9]);var inst_12812 = (state_12831[10]);var inst_12815 = (function (){var c = inst_12812;var v = inst_12811;var vec__12808 = inst_12810;var cs = inst_12803;return ((function (c,v,vec__12808,cs,inst_12803,inst_12810,inst_12811,inst_12812,state_val_12832,c__6346__auto___12855,out){
return (function (p1__12748_SHARP_){return cljs.core.not_EQ_.call(null,c,p1__12748_SHARP_);
});
;})(c,v,vec__12808,cs,inst_12803,inst_12810,inst_12811,inst_12812,state_val_12832,c__6346__auto___12855,out))
})();var inst_12816 = cljs.core.filterv.call(null,inst_12815,inst_12803);var inst_12803__$1 = inst_12816;var state_12831__$1 = (function (){var statearr_12842 = state_12831;(statearr_12842[7] = inst_12803__$1);
return statearr_12842;
})();var statearr_12843_12863 = state_12831__$1;(statearr_12843_12863[2] = null);
(statearr_12843_12863[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 9))
{var inst_12811 = (state_12831[9]);var state_12831__$1 = state_12831;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_12831__$1,11,out,inst_12811);
} else
{if((state_val_12832 === 10))
{var inst_12823 = (state_12831[2]);var state_12831__$1 = state_12831;var statearr_12845_12864 = state_12831__$1;(statearr_12845_12864[2] = inst_12823);
(statearr_12845_12864[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12832 === 11))
{var inst_12803 = (state_12831[7]);var inst_12820 = (state_12831[2]);var tmp12844 = inst_12803;var inst_12803__$1 = tmp12844;var state_12831__$1 = (function (){var statearr_12846 = state_12831;(statearr_12846[11] = inst_12820);
(statearr_12846[7] = inst_12803__$1);
return statearr_12846;
})();var statearr_12847_12865 = state_12831__$1;(statearr_12847_12865[2] = null);
(statearr_12847_12865[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12855,out))
;return ((function (switch__6331__auto__,c__6346__auto___12855,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12851 = [null,null,null,null,null,null,null,null,null,null,null,null];(statearr_12851[0] = state_machine__6332__auto__);
(statearr_12851[1] = 1);
return statearr_12851;
});
var state_machine__6332__auto____1 = (function (state_12831){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_12831);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12852){if((e12852 instanceof Object))
{var ex__6335__auto__ = e12852;var statearr_12853_12866 = state_12831;(statearr_12853_12866[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12831);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12852;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12867 = state_12831;
state_12831 = G__12867;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_12831){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_12831);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12855,out))
})();var state__6348__auto__ = (function (){var statearr_12854 = f__6347__auto__.call(null);(statearr_12854[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12855);
return statearr_12854;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12855,out))
);
return out;
});
merge = function(chs,buf_or_n){
switch(arguments.length){
case 1:
return merge__1.call(this,chs);
case 2:
return merge__2.call(this,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
merge.cljs$core$IFn$_invoke$arity$1 = merge__1;
merge.cljs$core$IFn$_invoke$arity$2 = merge__2;
return merge;
})()
;
/**
* Returns a channel containing the single (collection) result of the
* items taken from the channel conjoined to the supplied
* collection. ch must close before into produces a result.
*/
cljs.core.async.into = (function into(coll,ch){return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
* Returns a channel that will return, at most, n items from ch. After n items
* have been returned, or ch has been closed, the return chanel will close.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.take = (function() {
var take = null;
var take__2 = (function (n,ch){return take.call(null,n,ch,null);
});
var take__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___12960 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___12960,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___12960,out){
return (function (state_12937){var state_val_12938 = (state_12937[1]);if((state_val_12938 === 1))
{var inst_12914 = 0;var state_12937__$1 = (function (){var statearr_12939 = state_12937;(statearr_12939[7] = inst_12914);
return statearr_12939;
})();var statearr_12940_12961 = state_12937__$1;(statearr_12940_12961[2] = null);
(statearr_12940_12961[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 2))
{var inst_12914 = (state_12937[7]);var inst_12916 = (inst_12914 < n);var state_12937__$1 = state_12937;if(cljs.core.truth_(inst_12916))
{var statearr_12941_12962 = state_12937__$1;(statearr_12941_12962[1] = 4);
} else
{var statearr_12942_12963 = state_12937__$1;(statearr_12942_12963[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 3))
{var inst_12934 = (state_12937[2]);var inst_12935 = cljs.core.async.close_BANG_.call(null,out);var state_12937__$1 = (function (){var statearr_12943 = state_12937;(statearr_12943[8] = inst_12934);
return statearr_12943;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_12937__$1,inst_12935);
} else
{if((state_val_12938 === 4))
{var state_12937__$1 = state_12937;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_12937__$1,7,ch);
} else
{if((state_val_12938 === 5))
{var state_12937__$1 = state_12937;var statearr_12944_12964 = state_12937__$1;(statearr_12944_12964[2] = null);
(statearr_12944_12964[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 6))
{var inst_12932 = (state_12937[2]);var state_12937__$1 = state_12937;var statearr_12945_12965 = state_12937__$1;(statearr_12945_12965[2] = inst_12932);
(statearr_12945_12965[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 7))
{var inst_12919 = (state_12937[9]);var inst_12919__$1 = (state_12937[2]);var inst_12920 = (inst_12919__$1 == null);var inst_12921 = cljs.core.not.call(null,inst_12920);var state_12937__$1 = (function (){var statearr_12946 = state_12937;(statearr_12946[9] = inst_12919__$1);
return statearr_12946;
})();if(inst_12921)
{var statearr_12947_12966 = state_12937__$1;(statearr_12947_12966[1] = 8);
} else
{var statearr_12948_12967 = state_12937__$1;(statearr_12948_12967[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 8))
{var inst_12919 = (state_12937[9]);var state_12937__$1 = state_12937;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_12937__$1,11,out,inst_12919);
} else
{if((state_val_12938 === 9))
{var state_12937__$1 = state_12937;var statearr_12949_12968 = state_12937__$1;(statearr_12949_12968[2] = null);
(statearr_12949_12968[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 10))
{var inst_12929 = (state_12937[2]);var state_12937__$1 = state_12937;var statearr_12950_12969 = state_12937__$1;(statearr_12950_12969[2] = inst_12929);
(statearr_12950_12969[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_12938 === 11))
{var inst_12914 = (state_12937[7]);var inst_12924 = (state_12937[2]);var inst_12925 = (inst_12914 + 1);var inst_12914__$1 = inst_12925;var state_12937__$1 = (function (){var statearr_12951 = state_12937;(statearr_12951[7] = inst_12914__$1);
(statearr_12951[10] = inst_12924);
return statearr_12951;
})();var statearr_12952_12970 = state_12937__$1;(statearr_12952_12970[2] = null);
(statearr_12952_12970[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___12960,out))
;return ((function (switch__6331__auto__,c__6346__auto___12960,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_12956 = [null,null,null,null,null,null,null,null,null,null,null];(statearr_12956[0] = state_machine__6332__auto__);
(statearr_12956[1] = 1);
return statearr_12956;
});
var state_machine__6332__auto____1 = (function (state_12937){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_12937);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e12957){if((e12957 instanceof Object))
{var ex__6335__auto__ = e12957;var statearr_12958_12971 = state_12937;(statearr_12958_12971[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_12937);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e12957;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__12972 = state_12937;
state_12937 = G__12972;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_12937){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_12937);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___12960,out))
})();var state__6348__auto__ = (function (){var statearr_12959 = f__6347__auto__.call(null);(statearr_12959[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___12960);
return statearr_12959;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___12960,out))
);
return out;
});
take = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return take__2.call(this,n,ch);
case 3:
return take__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take.cljs$core$IFn$_invoke$arity$2 = take__2;
take.cljs$core$IFn$_invoke$arity$3 = take__3;
return take;
})()
;
/**
* Returns a channel that will contain values from ch. Consecutive duplicate
* values will be dropped.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.unique = (function() {
var unique = null;
var unique__1 = (function (ch){return unique.call(null,ch,null);
});
var unique__2 = (function (ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___13069 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___13069,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___13069,out){
return (function (state_13044){var state_val_13045 = (state_13044[1]);if((state_val_13045 === 1))
{var inst_13021 = null;var state_13044__$1 = (function (){var statearr_13046 = state_13044;(statearr_13046[7] = inst_13021);
return statearr_13046;
})();var statearr_13047_13070 = state_13044__$1;(statearr_13047_13070[2] = null);
(statearr_13047_13070[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 2))
{var state_13044__$1 = state_13044;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_13044__$1,4,ch);
} else
{if((state_val_13045 === 3))
{var inst_13041 = (state_13044[2]);var inst_13042 = cljs.core.async.close_BANG_.call(null,out);var state_13044__$1 = (function (){var statearr_13048 = state_13044;(statearr_13048[8] = inst_13041);
return statearr_13048;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_13044__$1,inst_13042);
} else
{if((state_val_13045 === 4))
{var inst_13024 = (state_13044[9]);var inst_13024__$1 = (state_13044[2]);var inst_13025 = (inst_13024__$1 == null);var inst_13026 = cljs.core.not.call(null,inst_13025);var state_13044__$1 = (function (){var statearr_13049 = state_13044;(statearr_13049[9] = inst_13024__$1);
return statearr_13049;
})();if(inst_13026)
{var statearr_13050_13071 = state_13044__$1;(statearr_13050_13071[1] = 5);
} else
{var statearr_13051_13072 = state_13044__$1;(statearr_13051_13072[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 5))
{var inst_13024 = (state_13044[9]);var inst_13021 = (state_13044[7]);var inst_13028 = cljs.core._EQ_.call(null,inst_13024,inst_13021);var state_13044__$1 = state_13044;if(inst_13028)
{var statearr_13052_13073 = state_13044__$1;(statearr_13052_13073[1] = 8);
} else
{var statearr_13053_13074 = state_13044__$1;(statearr_13053_13074[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 6))
{var state_13044__$1 = state_13044;var statearr_13055_13075 = state_13044__$1;(statearr_13055_13075[2] = null);
(statearr_13055_13075[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 7))
{var inst_13039 = (state_13044[2]);var state_13044__$1 = state_13044;var statearr_13056_13076 = state_13044__$1;(statearr_13056_13076[2] = inst_13039);
(statearr_13056_13076[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 8))
{var inst_13021 = (state_13044[7]);var tmp13054 = inst_13021;var inst_13021__$1 = tmp13054;var state_13044__$1 = (function (){var statearr_13057 = state_13044;(statearr_13057[7] = inst_13021__$1);
return statearr_13057;
})();var statearr_13058_13077 = state_13044__$1;(statearr_13058_13077[2] = null);
(statearr_13058_13077[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 9))
{var inst_13024 = (state_13044[9]);var state_13044__$1 = state_13044;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_13044__$1,11,out,inst_13024);
} else
{if((state_val_13045 === 10))
{var inst_13036 = (state_13044[2]);var state_13044__$1 = state_13044;var statearr_13059_13078 = state_13044__$1;(statearr_13059_13078[2] = inst_13036);
(statearr_13059_13078[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13045 === 11))
{var inst_13024 = (state_13044[9]);var inst_13033 = (state_13044[2]);var inst_13021 = inst_13024;var state_13044__$1 = (function (){var statearr_13060 = state_13044;(statearr_13060[10] = inst_13033);
(statearr_13060[7] = inst_13021);
return statearr_13060;
})();var statearr_13061_13079 = state_13044__$1;(statearr_13061_13079[2] = null);
(statearr_13061_13079[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___13069,out))
;return ((function (switch__6331__auto__,c__6346__auto___13069,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_13065 = [null,null,null,null,null,null,null,null,null,null,null];(statearr_13065[0] = state_machine__6332__auto__);
(statearr_13065[1] = 1);
return statearr_13065;
});
var state_machine__6332__auto____1 = (function (state_13044){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_13044);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e13066){if((e13066 instanceof Object))
{var ex__6335__auto__ = e13066;var statearr_13067_13080 = state_13044;(statearr_13067_13080[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_13044);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e13066;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__13081 = state_13044;
state_13044 = G__13081;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_13044){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_13044);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___13069,out))
})();var state__6348__auto__ = (function (){var statearr_13068 = f__6347__auto__.call(null);(statearr_13068[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___13069);
return statearr_13068;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___13069,out))
);
return out;
});
unique = function(ch,buf_or_n){
switch(arguments.length){
case 1:
return unique__1.call(this,ch);
case 2:
return unique__2.call(this,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unique.cljs$core$IFn$_invoke$arity$1 = unique__1;
unique.cljs$core$IFn$_invoke$arity$2 = unique__2;
return unique;
})()
;
/**
* Returns a channel that will contain vectors of n items taken from ch. The
* final vector in the return channel may be smaller than n if ch closed before
* the vector could be completely filled.
* 
* The output channel is unbuffered by default, unless buf-or-n is given
*/
cljs.core.async.partition = (function() {
var partition = null;
var partition__2 = (function (n,ch){return partition.call(null,n,ch,null);
});
var partition__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___13216 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___13216,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___13216,out){
return (function (state_13186){var state_val_13187 = (state_13186[1]);if((state_val_13187 === 1))
{var inst_13149 = (new Array(n));var inst_13150 = inst_13149;var inst_13151 = 0;var state_13186__$1 = (function (){var statearr_13188 = state_13186;(statearr_13188[7] = inst_13150);
(statearr_13188[8] = inst_13151);
return statearr_13188;
})();var statearr_13189_13217 = state_13186__$1;(statearr_13189_13217[2] = null);
(statearr_13189_13217[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 2))
{var state_13186__$1 = state_13186;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_13186__$1,4,ch);
} else
{if((state_val_13187 === 3))
{var inst_13184 = (state_13186[2]);var state_13186__$1 = state_13186;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_13186__$1,inst_13184);
} else
{if((state_val_13187 === 4))
{var inst_13154 = (state_13186[9]);var inst_13154__$1 = (state_13186[2]);var inst_13155 = (inst_13154__$1 == null);var inst_13156 = cljs.core.not.call(null,inst_13155);var state_13186__$1 = (function (){var statearr_13190 = state_13186;(statearr_13190[9] = inst_13154__$1);
return statearr_13190;
})();if(inst_13156)
{var statearr_13191_13218 = state_13186__$1;(statearr_13191_13218[1] = 5);
} else
{var statearr_13192_13219 = state_13186__$1;(statearr_13192_13219[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 5))
{var inst_13154 = (state_13186[9]);var inst_13150 = (state_13186[7]);var inst_13151 = (state_13186[8]);var inst_13159 = (state_13186[10]);var inst_13158 = (inst_13150[inst_13151] = inst_13154);var inst_13159__$1 = (inst_13151 + 1);var inst_13160 = (inst_13159__$1 < n);var state_13186__$1 = (function (){var statearr_13193 = state_13186;(statearr_13193[10] = inst_13159__$1);
(statearr_13193[11] = inst_13158);
return statearr_13193;
})();if(cljs.core.truth_(inst_13160))
{var statearr_13194_13220 = state_13186__$1;(statearr_13194_13220[1] = 8);
} else
{var statearr_13195_13221 = state_13186__$1;(statearr_13195_13221[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 6))
{var inst_13151 = (state_13186[8]);var inst_13172 = (inst_13151 > 0);var state_13186__$1 = state_13186;if(cljs.core.truth_(inst_13172))
{var statearr_13197_13222 = state_13186__$1;(statearr_13197_13222[1] = 12);
} else
{var statearr_13198_13223 = state_13186__$1;(statearr_13198_13223[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 7))
{var inst_13182 = (state_13186[2]);var state_13186__$1 = state_13186;var statearr_13199_13224 = state_13186__$1;(statearr_13199_13224[2] = inst_13182);
(statearr_13199_13224[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 8))
{var inst_13150 = (state_13186[7]);var inst_13159 = (state_13186[10]);var tmp13196 = inst_13150;var inst_13150__$1 = tmp13196;var inst_13151 = inst_13159;var state_13186__$1 = (function (){var statearr_13200 = state_13186;(statearr_13200[7] = inst_13150__$1);
(statearr_13200[8] = inst_13151);
return statearr_13200;
})();var statearr_13201_13225 = state_13186__$1;(statearr_13201_13225[2] = null);
(statearr_13201_13225[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 9))
{var inst_13150 = (state_13186[7]);var inst_13164 = cljs.core.vec.call(null,inst_13150);var state_13186__$1 = state_13186;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_13186__$1,11,out,inst_13164);
} else
{if((state_val_13187 === 10))
{var inst_13170 = (state_13186[2]);var state_13186__$1 = state_13186;var statearr_13202_13226 = state_13186__$1;(statearr_13202_13226[2] = inst_13170);
(statearr_13202_13226[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 11))
{var inst_13166 = (state_13186[2]);var inst_13167 = (new Array(n));var inst_13150 = inst_13167;var inst_13151 = 0;var state_13186__$1 = (function (){var statearr_13203 = state_13186;(statearr_13203[7] = inst_13150);
(statearr_13203[8] = inst_13151);
(statearr_13203[12] = inst_13166);
return statearr_13203;
})();var statearr_13204_13227 = state_13186__$1;(statearr_13204_13227[2] = null);
(statearr_13204_13227[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 12))
{var inst_13150 = (state_13186[7]);var inst_13174 = cljs.core.vec.call(null,inst_13150);var state_13186__$1 = state_13186;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_13186__$1,15,out,inst_13174);
} else
{if((state_val_13187 === 13))
{var state_13186__$1 = state_13186;var statearr_13205_13228 = state_13186__$1;(statearr_13205_13228[2] = null);
(statearr_13205_13228[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 14))
{var inst_13179 = (state_13186[2]);var inst_13180 = cljs.core.async.close_BANG_.call(null,out);var state_13186__$1 = (function (){var statearr_13206 = state_13186;(statearr_13206[13] = inst_13179);
return statearr_13206;
})();var statearr_13207_13229 = state_13186__$1;(statearr_13207_13229[2] = inst_13180);
(statearr_13207_13229[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13187 === 15))
{var inst_13176 = (state_13186[2]);var state_13186__$1 = state_13186;var statearr_13208_13230 = state_13186__$1;(statearr_13208_13230[2] = inst_13176);
(statearr_13208_13230[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___13216,out))
;return ((function (switch__6331__auto__,c__6346__auto___13216,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_13212 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_13212[0] = state_machine__6332__auto__);
(statearr_13212[1] = 1);
return statearr_13212;
});
var state_machine__6332__auto____1 = (function (state_13186){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_13186);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e13213){if((e13213 instanceof Object))
{var ex__6335__auto__ = e13213;var statearr_13214_13231 = state_13186;(statearr_13214_13231[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_13186);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e13213;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__13232 = state_13186;
state_13186 = G__13232;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_13186){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_13186);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___13216,out))
})();var state__6348__auto__ = (function (){var statearr_13215 = f__6347__auto__.call(null);(statearr_13215[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___13216);
return statearr_13215;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___13216,out))
);
return out;
});
partition = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition__2.call(this,n,ch);
case 3:
return partition__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition.cljs$core$IFn$_invoke$arity$2 = partition__2;
partition.cljs$core$IFn$_invoke$arity$3 = partition__3;
return partition;
})()
;
/**
* Returns a channel that will contain vectors of items taken from ch. New
* vectors will be created whenever (f itm) returns a value that differs from
* the previous item's (f itm).
* 
* The output channel is unbuffered, unless buf-or-n is given
*/
cljs.core.async.partition_by = (function() {
var partition_by = null;
var partition_by__2 = (function (f,ch){return partition_by.call(null,f,ch,null);
});
var partition_by__3 = (function (f,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__6346__auto___13375 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto___13375,out){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto___13375,out){
return (function (state_13345){var state_val_13346 = (state_13345[1]);if((state_val_13346 === 1))
{var inst_13304 = [];var inst_13305 = inst_13304;var inst_13306 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538);var state_13345__$1 = (function (){var statearr_13347 = state_13345;(statearr_13347[7] = inst_13305);
(statearr_13347[8] = inst_13306);
return statearr_13347;
})();var statearr_13348_13376 = state_13345__$1;(statearr_13348_13376[2] = null);
(statearr_13348_13376[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 2))
{var state_13345__$1 = state_13345;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_13345__$1,4,ch);
} else
{if((state_val_13346 === 3))
{var inst_13343 = (state_13345[2]);var state_13345__$1 = state_13345;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_13345__$1,inst_13343);
} else
{if((state_val_13346 === 4))
{var inst_13309 = (state_13345[9]);var inst_13309__$1 = (state_13345[2]);var inst_13310 = (inst_13309__$1 == null);var inst_13311 = cljs.core.not.call(null,inst_13310);var state_13345__$1 = (function (){var statearr_13349 = state_13345;(statearr_13349[9] = inst_13309__$1);
return statearr_13349;
})();if(inst_13311)
{var statearr_13350_13377 = state_13345__$1;(statearr_13350_13377[1] = 5);
} else
{var statearr_13351_13378 = state_13345__$1;(statearr_13351_13378[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 5))
{var inst_13313 = (state_13345[10]);var inst_13306 = (state_13345[8]);var inst_13309 = (state_13345[9]);var inst_13313__$1 = f.call(null,inst_13309);var inst_13314 = cljs.core._EQ_.call(null,inst_13313__$1,inst_13306);var inst_13315 = cljs.core.keyword_identical_QMARK_.call(null,inst_13306,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538));var inst_13316 = (inst_13314) || (inst_13315);var state_13345__$1 = (function (){var statearr_13352 = state_13345;(statearr_13352[10] = inst_13313__$1);
return statearr_13352;
})();if(cljs.core.truth_(inst_13316))
{var statearr_13353_13379 = state_13345__$1;(statearr_13353_13379[1] = 8);
} else
{var statearr_13354_13380 = state_13345__$1;(statearr_13354_13380[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 6))
{var inst_13305 = (state_13345[7]);var inst_13330 = inst_13305.length;var inst_13331 = (inst_13330 > 0);var state_13345__$1 = state_13345;if(cljs.core.truth_(inst_13331))
{var statearr_13356_13381 = state_13345__$1;(statearr_13356_13381[1] = 12);
} else
{var statearr_13357_13382 = state_13345__$1;(statearr_13357_13382[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 7))
{var inst_13341 = (state_13345[2]);var state_13345__$1 = state_13345;var statearr_13358_13383 = state_13345__$1;(statearr_13358_13383[2] = inst_13341);
(statearr_13358_13383[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 8))
{var inst_13313 = (state_13345[10]);var inst_13305 = (state_13345[7]);var inst_13309 = (state_13345[9]);var inst_13318 = inst_13305.push(inst_13309);var tmp13355 = inst_13305;var inst_13305__$1 = tmp13355;var inst_13306 = inst_13313;var state_13345__$1 = (function (){var statearr_13359 = state_13345;(statearr_13359[11] = inst_13318);
(statearr_13359[7] = inst_13305__$1);
(statearr_13359[8] = inst_13306);
return statearr_13359;
})();var statearr_13360_13384 = state_13345__$1;(statearr_13360_13384[2] = null);
(statearr_13360_13384[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 9))
{var inst_13305 = (state_13345[7]);var inst_13321 = cljs.core.vec.call(null,inst_13305);var state_13345__$1 = state_13345;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_13345__$1,11,out,inst_13321);
} else
{if((state_val_13346 === 10))
{var inst_13328 = (state_13345[2]);var state_13345__$1 = state_13345;var statearr_13361_13385 = state_13345__$1;(statearr_13361_13385[2] = inst_13328);
(statearr_13361_13385[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 11))
{var inst_13313 = (state_13345[10]);var inst_13309 = (state_13345[9]);var inst_13323 = (state_13345[2]);var inst_13324 = [];var inst_13325 = inst_13324.push(inst_13309);var inst_13305 = inst_13324;var inst_13306 = inst_13313;var state_13345__$1 = (function (){var statearr_13362 = state_13345;(statearr_13362[12] = inst_13325);
(statearr_13362[13] = inst_13323);
(statearr_13362[7] = inst_13305);
(statearr_13362[8] = inst_13306);
return statearr_13362;
})();var statearr_13363_13386 = state_13345__$1;(statearr_13363_13386[2] = null);
(statearr_13363_13386[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 12))
{var inst_13305 = (state_13345[7]);var inst_13333 = cljs.core.vec.call(null,inst_13305);var state_13345__$1 = state_13345;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_13345__$1,15,out,inst_13333);
} else
{if((state_val_13346 === 13))
{var state_13345__$1 = state_13345;var statearr_13364_13387 = state_13345__$1;(statearr_13364_13387[2] = null);
(statearr_13364_13387[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 14))
{var inst_13338 = (state_13345[2]);var inst_13339 = cljs.core.async.close_BANG_.call(null,out);var state_13345__$1 = (function (){var statearr_13365 = state_13345;(statearr_13365[14] = inst_13338);
return statearr_13365;
})();var statearr_13366_13388 = state_13345__$1;(statearr_13366_13388[2] = inst_13339);
(statearr_13366_13388[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_13346 === 15))
{var inst_13335 = (state_13345[2]);var state_13345__$1 = state_13345;var statearr_13367_13389 = state_13345__$1;(statearr_13367_13389[2] = inst_13335);
(statearr_13367_13389[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__6346__auto___13375,out))
;return ((function (switch__6331__auto__,c__6346__auto___13375,out){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_13371 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_13371[0] = state_machine__6332__auto__);
(statearr_13371[1] = 1);
return statearr_13371;
});
var state_machine__6332__auto____1 = (function (state_13345){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_13345);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e13372){if((e13372 instanceof Object))
{var ex__6335__auto__ = e13372;var statearr_13373_13390 = state_13345;(statearr_13373_13390[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_13345);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e13372;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__13391 = state_13345;
state_13345 = G__13391;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_13345){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_13345);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto___13375,out))
})();var state__6348__auto__ = (function (){var statearr_13374 = f__6347__auto__.call(null);(statearr_13374[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto___13375);
return statearr_13374;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto___13375,out))
);
return out;
});
partition_by = function(f,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition_by__2.call(this,f,ch);
case 3:
return partition_by__3.call(this,f,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition_by.cljs$core$IFn$_invoke$arity$2 = partition_by__2;
partition_by.cljs$core$IFn$_invoke$arity$3 = partition_by__3;
return partition_by;
})()
;

//# sourceMappingURL=async.js.map