(ns om-tut.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [pub put! chan <!]]
            [jayq.core :as jq]))

(enable-console-print!)

(defn image? [url]
  (= "jpg" (last (clojure.string/split url #"\."))))

(def app-state
  (atom {}))

(def global-chanel (chan))
(def action
  (pub global-chanel #(:action %)))

(defn contacts-view [app owner]
  (reify
    om/IInitState
    (init-state [_]
      (let [action (chan)]
        (jq/ajax "http://www.reddit.com/hot.json"
                 {:success (fn [data] (let [data (-> data (js->clj :keywordize-keys true)
                                                     :data :children)
                                            urls (map (fn [d] (-> d :data (select-keys [:title :url]))) data)]
                                        (om/transact! app :posts (fn [_] urls))
                                        (println app)))
                  :dataType "json"})
        (jq/bind (jq/$ :body)
                 :keydown (fn [e] (case (.-keyCode e)
                                    39 (put! action :next)
                                    37 (put! action :back))))
        {:action action}))
    om/IWillMount
    (will-mount [_]
      (let [action (om/get-state owner :action)]
        (go (loop []
              (let [action (<! action)]
                (println action)
                (om/transact! app :posts
                              (fn [p] (rest p)))
                (recur))))))
    om/IRenderState
    (render-state [this {:keys [action]}]
      (dom/div #js {:className "pure-g"}
               (dom/div #js {:className "pure-u-1-12" :id "left"} nil)
               (dom/div #js {:id "test"
                             :tabIndex 0
                             :className "pure-u-20-24"}
			(dom/h1 nil (-> app :posts first :title))
                        (let [src (-> app :posts first :url)]
                          (if (image? src)
                            (dom/img #js {:width "80%" :height "80%" :src src})
                            (dom/iframe #js {:src (-> app :posts first :url)
                                             :id "frame"
                                             :sandbox "allow-forms allow-scripts"} nil)))
                        (dom/button #js {:onClick (fn [_] (put! action :uno))} "Next"))
               (dom/div #js {:className "pure-u-1-12" :id "right"} nil))
      )))

(om/root contacts-view  app-state
         {:target (. js/document (getElementById "container"))})

